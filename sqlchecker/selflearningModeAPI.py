from pickle import NONE
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)

from sqlchecker.db import get_db, select_query_dict

from random import randint, shuffle, choice

from sqlchecker.test import write_solution_attempt

bp = Blueprint('test', __name__)


@bp.route('/wmode/self_learning',methods=('GET',))
def self_learning():
    return render_template('work_mode/self_learning.html')

@bp.route('/wmode/training/start',methods=('GET',))
def self_learning_start_get_drow():#кнопка начать.
    flag = 0
    info = self_learning_start_get(flag)

    exercise = info[0]
    id_assignment = info[1]
    
    if flag == 1 :
        return render_template('work_mode/have_unsolved_problems.html')
    if flag == 2:
        return render_template('work_mode/not_unavailable_for_training.html')

    return render_template('work_mode/training.html', exercise = exercise, id_assignment = id_assignment)


@bp.route('/wmode/training/continue',methods=('GET',))

def self_learning_continue_drow():#кнопка продолжить.

    
    flag = 0
    info = self_learning_continue(flag)

    number_of_ex_to_continue = info[0]
    id_assignment = info[1]

    if flag == 1:
        flash("вы еще не приступали к заданиям")
        return render_template('work_mode/self_learning.html')

    return render_template('work_mode/training.html', exercise = number_of_ex_to_continue, id_assignment = id_assignment)

@bp.route('/wmode/training/start',methods=('POST',))
@bp.route('/wmode/training/continue',methods=('POST',))
def self_learning_start_and_continue_post_drow():
    flag = 0
    error = None

    info = self_learning_start_and_continue_post(flag, error)
    number_of_ex_to_continue = info[0]
    id_assignment = info[1]
    
    if flag == 0:
        flash(error)
        return render_template('work_mode/training.html', exercise = number_of_ex_to_continue, id_assignment = id_assignment)

    return render_template('work_mode/self_learning.html')

def self_learning_start_get(flag):
    id_assignment = 0

    id_praktiki = 1 #int(request.form['id_praktiki'])
    db = get_db()
    
    number_unsolved_problems = select_query_dict(db,                                                                 
        'select assignment.id, solution_attempts.id_assignment '
        'from assignment '
        'join exercise on exercise.id = assignment.id_exercise '
        'left join solution_attempts on assignment.id = solution_attempts.id_assignment '
        'join student on student.id = assignment.id_student '
        'join category on category.id = exercise.category_id '
        'join users on users.id = student.id_user '
        'where (solution_attempts.id_assignment is null and users.id = %s and assignment.usage_mode = 0 and category.id = %s) ',[g.user['id'], id_praktiki])

    if len( number_unsolved_problems )> 0 :
        flag = 1
        return 1
    #выбираем id занятия, по которому хотим получить запрос.
    
    #запрос на проверку количество решенных задач учеником, если их меньше 2, то он не получает 
    if id_praktiki == 1:
        

        number_of_exes = select_query_dict(db,
            'SELECT exercise.id from exercise where exercise.category_id = %s', [id_praktiki])
        choice_of_ex = choice(number_of_exes)['id']

        CURRENT_DATE = select_query_dict(db,'select now()', NONE)[0]['now'] #дата решения
        CURRENT_TIME = select_query_dict(db,'select now()', NONE)[0]['now'] #дата решения
        cursor = db.cursor()
        cursor.execute(
            'INSERT INTO assignment (id_student, solving_data, time_start, id_exercise, usage_mode)'
            ' VALUES (%s, %s, %s, %s, %s) '
            'returning id',
            (g.user['id'], CURRENT_DATE,CURRENT_TIME, choice_of_ex, 0)
        )
        id_assignment = cursor.fetchone()[0]
        db.commit()
        cursor.close()

        exercise = select_query_dict(db,
            'SELECT * from exercise where id = %s and exercise.category_id = %s', [choice_of_ex, id_praktiki])[0]

    else: 
        
        id_user = g.user['id']
    
        number_of_resolved_ex = select_query_dict(db,         # запрос на количество решенных задач по предыдущей теме.(минимум 2)
        'select count(solution_attempts.id) '
        'from solution_attempts '
        'join assignment on assignment.id = solution_attempts.id_assignment '
        'join student on student.id = assignment.id_student '
        'join exercise on exercise.id = assignment.id_exercise '
        'join category on category.id = exercise.category_id '
        'join users on users.id = student.id_user '
        'where (solution_attempts.result = 0 and users.id = %s and category.id = %s)',[id_user, id_praktiki])[0]
        

        if number_of_resolved_ex['count'] >= 2:
            number_of_exes = select_query_dict(db,
                'SELECT exercise.id from exercise where exercise.category_id = %s', [id_praktiki])
            choice_of_ex = choice(number_of_exes)['id']

            CURRENT_DATE = select_query_dict(db,'select now()', NONE)[0]['now'] #дата решения
            CURRENT_TIME = select_query_dict(db,'select now()', NONE)[0]['now'] #дата решения
            cursor = db.cursor()
            cursor.execute(
                'INSERT INTO assignment (id_student, solving_data, time_start, id_exercise, usage_mode)'
                ' VALUES (%s, %s, %s, %s, %s) '
                'returning id',
                (g.user['id'], CURRENT_DATE,CURRENT_TIME, choice_of_ex, 0)
            )
            id_assignment = cursor.fetchone()[0]
            db.commit()
            cursor.close()

            exercise = select_query_dict(db,
                'SELECT * from exercise where id = %s and exercise.category_id = %s', [choice_of_ex, id_praktiki])[0]
        else:
            flag = 2
            return 2

    return exercise, id_assignment

def self_learning_continue(flag):

    id_praktiki = 1 #int(request.form['id_praktiki'])
    id_assignment = 0
    db = get_db()
    id_user = g.user['id']

    number_of_ex_to_continue = select_query_dict(db,                                                                 
        'select exercise.* '
        'from assignment '
        'join exercise on exercise.id = assignment.id_exercise '
        'left join solution_attempts on assignment.id = solution_attempts.id_assignment '
        'join student on student.id = assignment.id_student '
        'join category on category.id = exercise.category_id '
        'join users on users.id = student.id_user '
        'where (solution_attempts.id_assignment is null and users.id = %s and assignment.usage_mode = 0 and category.id = %s) ',[g.user['id'], id_praktiki])
    if len(number_of_ex_to_continue) == 0:
        flag = 1
        return 1

    id_assignment = select_query_dict(db,                                                                 
        'select assignment.id '
        'from assignment '
        'join student on student.id = assignment.id_student '
        'join users on users.id = student.id_user '
        'left join solution_attempts on assignment.id = solution_attempts.id_assignment '
        'where  solution_attempts.id_assignment is null and assignment.usage_mode = 0 and users.id = %s', [g.user['id']])[0]

    return number_of_ex_to_continue[0], id_assignment['id']

def self_learning_start_and_continue_post(flag, error):
    db = get_db()

    body = request.form['body']

    error = None

    if not body:
        error = 'Вы не ввели ответ'
        number_of_ex_to_continue = select_query_dict(db,                                                                 
        'select exercise.* '
        'from assignment '
        'join exercise on exercise.id = assignment.id_exercise '
        'where assignment.id = %s ',[request.form['id_assignment']])
 
    if error is not None:
        flag = 0
        return number_of_ex_to_continue[0], request.form['id_assignment']
    else:
        
        id_ass = request.form['id_assignment']
        
        write_solution_attempt(int(id_ass), body, 0)
        flag = 1
        return 1