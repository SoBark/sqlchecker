
from pickle import NONE
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)

from sqlchecker.db import get_db, select_query_dict

from random import randint, shuffle, choice

from sqlchecker.test import write_solution_attempt

bp = Blueprint('test', __name__)

@bp.route('/wmode/debug',methods=('get',))
def debug_get():
    return render_template('work_mode/debug.html')

@bp.route('/wmode/debug/start',methods=('GET',))
def debug_start_get_drow():
    flag = 0
    exercise = debug_start_get(flag)
    
    if(flag == 1):
        return render_template('work_mode/have_unsolved_problems_debug.html')
        
    exercise = [{}]
    return render_template('work_mode/debug_start.html', exercise = exercise)

@bp.route('/wmode/debug/continue',methods=('GET',))
def debug_continue_get_drow():
    flag = 0
    exercise = debug_continue_get(flag)                                                                 
    if flag == 1:
        flash("вы еще не приступали к заданиям")
        return render_template('work_mode/debug.html')
    
    return render_template('work_mode/debug_continue.html', exercise = exercise)


@bp.route('/wmode/debug/start',methods=('POST',))
def debug_start_post_drow():
    flag = 0

    
    exercise = debug_start_post(flag)
    

    if flag == 1:
        flash("вы не полностью ввели данные для выборки задания")   
    if flag == 2:
        flash("вы неверно ввели данные для выборки задания")
    
    if flag == 6:
        return render_template('work_mode/debug.html')
        
    return render_template('work_mode/debug_start.html', exercise = exercise)

@bp.route('/wmode/debug/continue',methods=('POST',))
def debug_continue_post_drow():
    flag = 0
    error = None
    number_of_ex_to_continue = debug_continue_post(flag, error)
    
    if flag == 1:
        flash(error)
        return render_template('work_mode/debug_continue.html', exercise = number_of_ex_to_continue)
    return render_template('work_mode/debug.html')

def debug_start_get(flag):
    db = get_db()

    id_assignment = select_query_dict(db,                                                                 
        'select assignment.id '
        'from assignment '
        'join student on student.id = assignment.id_student '
        'join users on users.id = student.id_user '
        'left join solution_attempts on assignment.id = solution_attempts.id_assignment '
        'where  solution_attempts.id_assignment is null and assignment.usage_mode = 1 and users.id = %s ', [g.user['id']])
    
    if(len(id_assignment)):
        flag = 1
        return 1
        
    exercise = [{}]

    return exercise

def debug_continue_get(flag):
    db = get_db()
    exercise = select_query_dict(db,                                                                 
        'select exercise.* '
        'from assignment '
        'left join solution_attempts on assignment.id = solution_attempts.id_assignment '
        'join student on student.id = assignment.id_student '
        'join users on users.id = student.id_user '
        'join exercise on exercise.id = assignment.id_exercise '
        'join category on category.id = exercise.category_id '
        'where  solution_attempts.id_assignment is null and assignment.usage_mode = 1 and users.id = %s ', [g.user['id']])
    if len(exercise) == 0:
        flag = 1
        return 1
    
    return exercise

@bp.route('/wmode/debug/start',methods=('POST',))
def debug_start_post(flag):

    db = get_db()
    exercise = [{}]
    

    number_lr = request.form['number_lr']
    number_primer = request.form['number_primer']
    assignment_number_lr = request.form['assignment_number_lr']
    body = request.form['body']

    if number_lr == '' or number_primer == '' or assignment_number_lr == '':
        flag = 1
        return 1   
    try:
        int(number_lr)
        int(number_primer)
        int(assignment_number_lr)
    except ValueError:
        flag = 2
        return 2

    exercise = select_query_dict(db,'select exercise.* from exercise   '
                            'join category on category.id = exercise.category_id  '
                            'where category.number_primer = %s and category.number_lr = %s and category.assignment_number_lr = %s ', [number_primer, number_lr, assignment_number_lr ])
    if len(exercise) == 0:
        flag = 3
        exercise = [{}]
        return exercise
       
    
    if len(request.form) == 5:
        id_assignment = select_query_dict(db,                                                                 
        'select assignment.id '
        'from assignment '
        'join student on student.id = assignment.id_student '
        'join users on users.id = student.id_user '
        'left join solution_attempts on assignment.id = solution_attempts.id_assignment '
        'where  solution_attempts.id_assignment is null and assignment.usage_mode = 1 and users.id = %s ', [g.user['id']])
        if (len(id_assignment)):
            
            exid = exercise[0]['id']
            cursor = db.cursor()
            cursor.execute('update assignment set id_exercise = %s where id = %s ', (exid, id_assignment[0]['id']))
            db.commit()
            cursor.close()
            flag = 4
            return exercise
        if request.form['take_for_debbag'] == 'взять задание на отладку':
                CURRENT_DATE = select_query_dict(db,'select now()', NONE)[0]['now'] #дата решения
                CURRENT_TIME = select_query_dict(db,'select now()', NONE)[0]['now'] #дата решения
                cursor = db.cursor()
                cursor.execute(
                    'INSERT INTO assignment (id_student, solving_data, time_start, id_exercise, usage_mode)'
                    ' VALUES (%s, %s, %s, %s, %s) '
                    'returning id',
                    (g.user['id'], CURRENT_DATE,CURRENT_TIME, exercise[0]['id'], 1)
                )
                id_assignment = cursor.fetchone()[0]
                db.commit()
                cursor.close()

    if len(request.form['body']) == 0:
        flag = 5
        return exercise
    else:
        id_assignment = select_query_dict(db,                                                                 
        'select assignment.id '
        'from assignment '
        'join student on student.id = assignment.id_student '
        'join users on users.id = student.id_user '
        'left join solution_attempts on assignment.id = solution_attempts.id_assignment '
        'where  solution_attempts.id_assignment is null and assignment.usage_mode = 1 and users.id = %s ', [g.user['id']])[0]
        
        write_solution_attempt(int(id_assignment['id']), request.form['body'], 0)
        
        flag = 6
        return 1

def debug_continue_post(flag, error):
    db = get_db()

    
    body = request.form['body']

    id_assignment = select_query_dict(db,                                                                 
        'select assignment.id '
        'from assignment '
        'join student on student.id = assignment.id_student '
        'join users on users.id = student.id_user '
        'left join solution_attempts on assignment.id = solution_attempts.id_assignment '
        'where  solution_attempts.id_assignment is null and assignment.usage_mode = 1 and users.id = %s ', [g.user['id']])

    if not body:
        error = 'Вы не ввели ответ'
        number_of_ex_to_continue = select_query_dict(db,                                                                 
        'select exercise.* '
        'from assignment '
        'join exercise on exercise.id = assignment.id_exercise '
        'where assignment.id = %s ',[int(id_assignment[0]['id'])])
    if error is not None:
        flag = 1
        return number_of_ex_to_continue
    else:
        
        write_solution_attempt(int(id_assignment[0]['id']), body, 0)
        return 1
