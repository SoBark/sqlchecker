from pickle import NONE
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)

from sqlchecker.db import get_db, select_query_dict

from random import randint, shuffle, choice

from sqlchecker.test import write_solution_attempt

bp = Blueprint('test', __name__)


@bp.route('/wmode/defence',methods=('get',))
def defence_get():
    return render_template('work_mode/defence.html')

@bp.route('/wmode/defence/choose',methods=('GET','POST'))
def defence_choose_get_drow():
    if request.method == 'GET':
        db = get_db()
        id_assignment = select_query_dict(db,                                                                 
        'select assignment.id '
        'from assignment '
        'join student on student.id = assignment.id_student '
        'join users on users.id = student.id_user '
        'left join solution_attempts on assignment.id = solution_attempts.id_assignment '
        'where solution_attempts.id_assignment is null and assignment.usage_mode = 3 and users.id = %s ', [g.user['id']])
        print(len(id_assignment))
        if(len(id_assignment)):
            return render_template('work_mode/have_unsolved_problems_defence.html')
        else:
            return render_template('work_mode/defence_choose.html')
    if request.method == 'POST':
        number_lr = request.form['number_lr']
        return redirect(url_for('test.defence_start_get_drow', number_lr = number_lr))
        

@bp.route('/wmode/defence/start',methods=('GET',))
def defence_start_get_drow(): #доступные для студента задания защиты лаб
    db = get_db()
    print()
    #consist_id = request.form['number_lr']
    exercise_ids = select_query_dict(db,                                                                 
        'select distinct(exercise.id) '
        'from exercise '
        'join category on category.id = exercise.category_id '
        'where category.id in(select category.id '
        'from category '
        'where category.number_lr = %s) and exercise.is_protection = 1 '
        'except '
        'select assignment.id_exercise '
        'from assignment '
        'join student on student.id = assignment.id_student '
        'join users on users.id = student.id_user '
        'where usage_mode = 3 and student.grp = %s', [int(request.args['number_lr']),'pmi91'])
    
    ids =[]
    for i in exercise_ids:
        ids.append(i['id'])
    choice_of_ex = choice(ids)

    CURRENT_DATE = select_query_dict(db,'select now()', NONE)[0]['now'] #дата решения
    CURRENT_TIME = select_query_dict(db,'select now()', NONE)[0]['now'] #дата решения
    cursor = db.cursor()
    cursor.execute(
        'INSERT INTO assignment (id_student, solving_data, time_start, id_exercise, usage_mode)'
        ' VALUES (%s, %s, %s, %s, %s) ',
        (g.user['id'], CURRENT_DATE,CURRENT_TIME, choice_of_ex, 3))
    db.commit()
    cursor.close()

    exercise = select_query_dict(db, 'select exercise.* from exercise where exercise.id = %s',[choice_of_ex])

    return render_template('work_mode/defence_start.html',  exercise = exercise[0])

@bp.route('/wmode/defence/continue',methods=('GET',))
def defence_continue_get_drow():
    db = get_db()
    exercise = select_query_dict(db,                                                                 
        'select exercise.* '
        'from assignment '
        'left join solution_attempts on assignment.id = solution_attempts.id_assignment '
        'join student on student.id = assignment.id_student '
        'join users on users.id = student.id_user '
        'join exercise on exercise.id = assignment.id_exercise '
        'join category on category.id = exercise.category_id '
        'where  solution_attempts.id_assignment is null and assignment.usage_mode = 3 and users.id = %s ', [g.user['id']])
    if len(exercise) == 0:
         flash("вы еще не приступали к заданиям")
         return render_template('work_mode/defence.html')
    
    return render_template('work_mode/defence_continue.html', exercise = exercise[0])

@bp.route('/wmode/defence/start',methods=('POST',))
@bp.route('/wmode/defence/continue',methods=('POST',))
def defence_start_and_continue_post_drow():
    db = get_db()

    body = request.form['body']

    error = None
    id_assignment = select_query_dict(db,                                                                 
        'select assignment.id '
        'from assignment '
        'join student on student.id = assignment.id_student '
        'join users on users.id = student.id_user '
        'left join solution_attempts on assignment.id = solution_attempts.id_assignment '
        'where  solution_attempts.id_assignment is null and assignment.usage_mode = 3 and users.id = %s ', [g.user['id']])

    if not body:
        error = 'Вы не записали ответ'
        number_of_ex_to_continue = select_query_dict(db,                                                                 
        'select exercise.* '
        'from assignment '
        'join exercise on exercise.id = assignment.id_exercise '
        'where assignment.id = %s ',[int(id_assignment[0]['id'])])
    if error is not None:
        flash(error)
        return render_template('work_mode/defence_continue.html', exercise = number_of_ex_to_continue[0])
    else:
            
        write_solution_attempt(int(id_assignment[0]['id']), body, 0)
        return render_template('work_mode/defence.html')

