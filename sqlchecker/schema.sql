DROP TABLE IF EXISTS posts CASCADE;
DROP TABLE IF EXISTS correct_results CASCADE;
DROP TABLE IF EXISTS correct_answers CASCADE;
DROP TABLE IF EXISTS answers CASCADE;
DROP TABLE IF EXISTS users CASCADE;
DROP TABLE IF EXISTS questions CASCADE;
DROP TABLE IF EXISTS videos CASCADE;
DROP TABLE IF EXISTS subtopics CASCADE;
DROP TABLE IF EXISTS topics CASCADE;
DROP TABLE IF EXISTS assignment CASCADE;
DROP TABLE IF EXISTS student CASCADE;
DROP TABLE IF EXISTS solution_attempts CASCADE;
DROP TABLE IF EXISTS exercise CASCADE;
DROP TABLE IF EXISTS category CASCADE;
DROP TABLE IF EXISTS alternative_sets_of_tables CASCADE;
DROP TABLE IF EXISTS ability_defence CASCADE;
DROP TABLE IF EXISTS test_exercises CASCADE;



CREATE TABLE users (
  id serial PRIMARY KEY,
  email TEXT UNIQUE NOT NULL,
  username TEXT UNIQUE NOT NULL,
  password TEXT NOT NULL,
  role TEXT NOT NULL
);

CREATE TABLE topics (
    id serial PRIMARY KEY,
    topic_name TEXT NOT NULL,
    topic_description TEXT
);

--subtopic_type 0 - теория
CREATE TABLE subtopics (
    id serial PRIMARY KEY,
    topic_id INTEGER NOT NULL,
    subtopic_type INTEGER NOT NULL,
    subtopic_order INTEGER NOT NULL,
    subtopic_name TEXT NOT NULL,
    subtopic_description TEXT,
    FOREIGN KEY (topic_id) REFERENCES topics(id)
    --Добавить условие уникальности внутри топика для ORDER
);

CREATE TABLE questions (

  id serial PRIMARY KEY,
  subtopic_id INTEGER,
  question_type INTEGER NOT NULL,
  body TEXT NOT NULL,
  FOREIGN KEY (subtopic_id) REFERENCES subtopics (id)
);

--сделать enumerate для статусов
CREATE TABLE answers (
  id serial PRIMARY KEY,
  body TEXT NOT NULL,
  check_status CHAR NOT NULL,
  usr_id INTEGER NOT NULL,
  question_id INTEGER NOT NULL,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  result TEXT DEFAULT NULL,
  FOREIGN KEY (usr_id) REFERENCES users (id),
  FOREIGN KEY (question_id) REFERENCES questions (id)
);

-- CREATE TABLE posts (
--   id serial PRIMARY KEY,
--   author_id INTEGER NOT NULL,
--   created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
--   title TEXT NOT NULL,
--   body TEXT NOT NULL,
--   FOREIGN KEY (author_id) REFERENCES users (id)
-- );

CREATE TABLE correct_answers(
    id serial PRIMARY KEY,
    correct_answer TEXT NOT NULL,
    preset_name TEXT DEFAULT NULL,
    question_id INTEGER NOT NULL,
    FOREIGN KEY (question_id) REFERENCES questions (id)
    
);


CREATE TABLE videos (
    id serial PRIMARY KEY,
    subtopic_id INTEGER NOT NULL,
    video_name TEXT NOT NULL,
    FOREIGN KEY (subtopic_id) REFERENCES subtopics(id)
);

CREATE TABLE student (
    -- id serial PRIMARY KEY,
    ID_CIU INTEGER NOT NULL,
    id INTEGER NOT NULL PRIMARY KEY, -- выбираем по этому это ID_CIU_UNIQUE
    F TEXT NOT NULL,
    N TEXT NOT NULL,
    P TEXT NOT NULL,
    TP TEXT NOT NULL,
    FK_STUDY_GROUP TEXT NOT NULL,
    WS_ID INTEGER NOT NULL
);

CREATE TABLE category (   
    -- id integer not null,
    id serial PRIMARY KEY,    
    category_name TEXT,
    descript character varying(256),
    number_primer integer,
    number_lr INTEGER,
    assignment_number_lr INTEGER,
    task character varying(4000)
);

CREATE TABLE exercise ( --задача
    -- id integer not null,
    id serial PRIMARY KEY,
    problem_statement character varying(4000) not null,
    category_id INTEGER,
    variant_number_lr INTEGER,
    is_protection integer default 0,
    bd integer default 1,
    is_sort integer default 0,
    right_result JSON,   
    FOREIGN KEY (category_id) REFERENCES category(id)
);
CREATE TABLE assignment (--задание
    id serial PRIMARY KEY,
    id_student INTEGER NOT NULL,
    solving_data TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    time_start TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    id_exercise INTEGER NOT NULL,
    usage_mode INTEGER NOT NULL,
    -- FOREIGN KEY (id_student) REFERENCES student(id),
    FOREIGN KEY (id_exercise) REFERENCES exercise(id)
);

CREATE TABLE solution_attempts (
    id serial PRIMARY KEY,
    id_assignment INTEGER NOT NULL,
    sql_query TEXT NOT NULL,
    posting_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    condition INTEGER NOT NULL,
    result INTEGER,
    message_error TEXT,
    execution_time DECIMAL,
    FOREIGN KEY (id_assignment) REFERENCES assignment(id)
);

CREATE TABLE alternative_sets_of_tables (
    id integer not null,
    textsql character varying(2000),
    id_exercise INTEGER,
    right_result JSON,
    FOREIGN KEY (id_exercise) REFERENCES exercise(id)
);

CREATE TABLE ability_defence(
     id serial PRIMARY KEY,
     id_student INTEGER,
     id_exercise INTEGER,
    FOREIGN KEY (id_exercise) REFERENCES exercise(id),
    FOREIGN KEY (id_student) REFERENCES student(id)
);

CREATE TABLE test_exercises(
     id serial PRIMARY KEY,
     id_student INTEGER,
     id_exercise INTEGER,
    FOREIGN KEY (id_exercise) REFERENCES exercise(id),
    FOREIGN KEY (id_student) REFERENCES student(id)
);






-- INSERT INTO topics(topic_name)
-- VALUES
-- ('Выборка данных из нескольких таблиц, внутреннее соединение таблиц');

-- INSERT INTO subtopics (topic_id, subtopic_type, subtopic_order, subtopic_name)
-- VALUES
-- (1, 0, 1, 'Простые условия отбора, вывод без повторений, сортировка результата');

-- INSERT INTO questions (subtopic_id, question_type, body)
-- VALUES
--   (1, 0,'Задание 1
-- Вывести о каждой поставке информацию: 
-- •	город, откуда сделана поставка, 
-- •	город, куда поставлены детали,
-- •	количество деталей,
-- •	вес детали.
-- Упорядочить  по  городу,  откуда сделана  поставка,  и  количеству деталей.');
-- --   ('Вопрос 3. Впишите свой sql запрос:'),
-- --   ('Вопрос 4. Впишите свой sql запрос:');

-- INSERT INTO  correct_answers (question_id, correct_answer)
-- VALUES
--     (1,'select  p.town   townp,  j.town  townj, spj.kol,  p.ves
-- from  spj
-- join  p  on  p.n_det = spj.n_det
-- join  j  on  j.n_izd = spj.n_izd
-- order  by 1,3'); 

insert into users values (1,'ilya@mail.com', 'ilya','pbkdf2:sha256:260000$Oi5C9QprQjM0tIrD$0b95fa177aa67da419dfbe389857860db63ed11a09df28e7e42accfeddf791d7', 'student');
insert into users values (2,'egor@mail.com', 'egor','pbkdf2:sha256:260000$Oi5C9QprQjM0tIrD$0b95fa177aa67da419dfbe389857860db63ed11a09df28e7e42accfeddf791d7', 'student');

insert into student values (472550, 233701, 'Истомин','Илья', 'Андреевич', 'Студент',33252, 200 );
insert into student values (489401, 249972, 'Мельников','Святослав', 'Константинович', 'Студент',33253, 200 );
insert into student values (464228, 227031, 'Жарков','Федор', 'Николаевич', 'Студент',33252, 200 );

insert into topics values (1, '1. Выборка данных из нескольких таблиц с использованием внутреннего соединения таблиц.', NULL);
insert into topics values (2, '2. Использование агрегатных функций без группирования данных', NULL);
insert into topics values (3, '3. Написание SQL-запросов с условиями отбора с некоррелированным многоуровневым подзапросом', NULL);
insert into topics values (4, '4. Написание SQL-запросов использованием операций над множествами (union, except, intersect).', NULL);
insert into topics values (5, '5. Выбор объектов, имеющих связь только с элементами подмножества, определяемого подзапросом без группировки.', NULL);
insert into topics values (6, '6. Выбор объектов, имеющих связь только с элементами подмножества, определяемого подзапросом с группировкой.', NULL);
insert into topics values (7, '7. Написание SQL-запросов, использующих внешнее соединение таблиц и подзапросов.', NULL);
insert into topics values (8, '8. Написание SQL-запросов, использующих внешнее соединение таблиц и подзапросов и конструкцией case.', NULL);
insert into topics values (9, '9. Написание SQL-запросов, использующих двойное агрегирование.    ', NULL);
insert into topics values (10, '10. Написание SQL-запросов, использующих коррелированные подзапросы.', NULL);
insert into topics values (11, '11. Выбор объектов, имеющих связь с каждым элементом определенного подмножества.', NULL);
insert into topics values (12, '12. Написание SQL-запросов, использующих конструкцию case.', NULL);
insert into topics values (13, '13. Выбор объектов, не имеющих связи ни с одним элементом определенного подмножества.', NULL);
insert into topics values (14, '14. Написание SQL-запросов, использующих внутреннее соединение таблиц и вложенных подзапросов с группировкой и агрегированием данных.', NULL);
insert into topics values (15, '15. Написание SQL-запросов, использующих внутреннее соединение таблиц и вложенных подзапросов с группировкой по нескольким полям.', NULL);
insert into topics values (16, '16. Написание SQL-запросов, использующих процентное распределение.', NULL);


--ЗАПОЛНЕНИЕ ТАБЛИЦ ЗАДАЧАМИ

insert into category(id,  category_name, descript, number_primer, number_lr, assignment_number_lr, task)
values (1, 'ПРИМЕР 1', 'Основной навык :  выборка данных из нескольких таблиц, внутреннее соединение таблиц.' || chr(10) || 'Дополнительный : простые условия  отбора,  вывод  без повторений,  сортировка  результата.' || chr(10) || '', 1, 2, 2, 'Найти поставщиков,  которые поставляли детали красного цвета  для изделий с длиной названия >8. Вывести номера поставщиков без повторений в порядке возрастания номеров.');
insert into category(id,  category_name, descript, number_primer, number_lr, assignment_number_lr, task)
values (2, 'ПРИМЕР 2,4', 'Основной навык:  использование  агрегатных функций без группирования данных' || chr(10) || 'Дополнительный: условия  отборас вложенными  некоррелированными подзапросами.' || chr(10) || '', 2, 4, 1, 'Выдать число изделий, для которых поставлялись детали из города, где проживает поставщик с максимальным рейтингом. ');
insert into category(id,  category_name, descript, number_primer, number_lr, assignment_number_lr, task)
values (3, 'ПРИМЕР 3', 'Основной навык :условия  отбора с некоррелированным многоуровневым  подзапросом' || chr(10) || 'Дополнительный :выборка данных из нескольких таблиц, внутреннее соединение таблиц' || chr(10) || '', 3, 2, 1, 'Выбрать изделия,  для которых поставлялись красные детали,   поставлявшиеся для изделия c названием длиной больше 11');
insert into category(id,  category_name, descript, number_primer, number_lr, assignment_number_lr, task)
values (4, 'ПРИМЕР 5', 'Основной навык:  использование  операций над множествами  union except intersect.' || chr(10) || 'Дополнительный: использование группирования и  условия  отбора групп.', 4, 2, 5, 'Построить таблицу со списком городов таких, что в городе вместе с поставщиком  размещаются либо только детали, либо только изделия, но не то и другое вместе.');
insert into category(id,  category_name, descript, number_primer, number_lr, assignment_number_lr, task)
values (5, 'ПРИМЕР 6', 'Основной навык:  Выбор объектов, имеющих  связь    только  с элементами  определенного подмножества.' || chr(10) || 'Дополнительный:  использование операции  разности множеств,  вложенных подзапросов.', 5, 2, 3, 'Получить список  деталей,  которые поставляли  ТОЛЬКО  поставщики,  выполнившие поставки в Рим. ');
insert into category(id,  category_name, descript, number_primer, number_lr, assignment_number_lr, task)
values (6, 'ПРИМЕР 7', 'Основной навык:  Выбор объектов, имеющих  связь    только  с элементами  определенного подмножества.' || chr(10) || 'Дополнительный:  использование операции  разности множеств и вложенных подзапросов с группировкой и  условием  отбора групп', 6, 4, 5, 'Выдать полную информацию о деталях,  которые поставлялись ТОЛЬКО для изделий  с числом поставок не менее 5. ');
insert into category(id,  category_name, descript, number_primer, number_lr, assignment_number_lr, task)
values (7, 'ПРИМЕР 8', 'Основной навык :  внешнее соединение таблиц  и подзапросов.' || chr(10) || 'Дополнительный : группирование и агрегатные функции', 7, 2, 4, 'Вывести полный список  городов   и  для каждого города  найти общий вес деталей красного цвета, поставленных в этот город.  Города в списке  должны быть ВСЕ. Список должен быть упорядочен по алфавиту.');
insert into category(id,  category_name, descript, number_primer, number_lr, assignment_number_lr, task)
values (8, 'ПРИМЕР 9', 'Основной навык :  внешнее соединение таблиц  и подзапросов.' || chr(10) || 'Дополнительный :  группирование и агрегатные функции, конструкция case  ', 8, 6, 3, 'Для каждой детали вывести её номер и средний объем поставок  этой детали поставщиком  S2  или 0, если поставок не было.  Детали в списке  должны быть ВСЕ. Список должен быть упорядочен по номеру детали.');
insert into category(id,  category_name, descript, number_primer, number_lr, assignment_number_lr, task)
values (9, 'ПРИМЕР 10', 'Основной навык:  двойное агрегирование.      ' || chr(10) || 'Дополнительный: условия  отбора с вложенными  некоррелированными подзапросами.', 9, 5, 1, 'Получить  средний вес  поставки  для каждого изделия  и найти их среднее.');
insert into category(id,  category_name, descript, number_primer, number_lr, assignment_number_lr, task)
values (10, 'ПРИМЕР 11', 'Основной навык:  коррелированный  подзапрос' || chr(10) || 'Дополнительный:  агрегатные функкции,  сортировка,  ограничение вывода(limit).', 10, 2, 6, 'Каждое изделие, в названии которого есть буква «к»,  перевести в город, в котором проживает поставщик, сделавший для изделия наименьшую по объему поставку. Если таких городов больше одного, перевести в первый по алфавиту из этих городов.');
insert into category(id,  category_name, descript, number_primer, number_lr, assignment_number_lr, task)
values (11, 'ПРИМЕР 12', 'Основной навык:  Выбор объектов, имеющих  связь  с каждым элементом  определенного подмножества.' || chr(10) || 'Дополнительный: использование группирования и  условия  отбора групп.', 11, 3, 2, 'Получить номера изделий, для которых поставлялась КАЖДАЯ деталь из списка поставщика S2.');
insert into category(id,  category_name, descript, number_primer, number_lr, assignment_number_lr, task)
values (12, 'ПРИМЕР 13', 'Основной навык :  конструкция case' || chr(10) || 'Дополнительный :  сортировка и ограничение вывода,  агрегатные функции  ', 12, 4, 2, 'Поменять местами города, где проживают поставщики с минимальным и максимальным рейтингом, т.е. поставщиков с минимальным рейтингом перевести в город, где проживает поставщик с максимальным рейтингом, и наоборот, поставщиков с максимальным рейтингом перевести в город, где проживает поставщик с минимальным рейтингом. Если городов несколько, брать первый по алфавиту из этих городов.');
insert into category(id,  category_name, descript, number_primer, number_lr, assignment_number_lr, task)
values (13, 'ПРИМЕР 14', 'Основной навык:  Выбор объектов, не имеющих  связи  ни с одним элементом  определенного подмножества.' || chr(10) || 'Дополнительный: использование операции  разности множеств и вложенных подзапросов ', 13, 4, 4, 'Выбрать города, в которые не делал поставок ни один поставщик, поставлявший зеленые детали.');
insert into category(id,  category_name, descript, number_primer, number_lr, assignment_number_lr, task)
values (14, 'ПРИМЕР 15', 'Основной навык :  внутреннее соединение таблиц и подзапросов.' || chr(10) || 'Дополнительный: использование вложенных подзапросов  с группировкой и агрегированием данных', 14, 4, 3, 'Найти  поставщиков, имеющих  поставки, вес  которых  составляет  менее 20% от максимального веса  поставки красной детали, выполненной  этим поставщиком.  Вывести номер поставщика,  вес поставки,  максимальный вес  поставки красной детали сделанной поставщиком.');
insert into category(id,  category_name, descript, number_primer, number_lr, assignment_number_lr, task)
values (15, 'ПРИМЕР 16', 'Основной навык :  внутреннее соединение таблиц и подзапросов.' || chr(10) || 'Дополнительный: использование подзапросов  с группировкой по нескольким полям ', 15, 5, 2, 'Для каждого поставщика из Парижа  найти число  поставок каждой  детали, им поставлявшейся. Вывести номер поставщика, город  поставщика, номер детали, название детали, число поставок.');
insert into category(id,  category_name, descript, number_primer, number_lr, assignment_number_lr, task)
values (16, 'ПРИМЕР 17', ' Процентное распределение', 16, 5, 3, 'Найти города поставщиков, поставлявших  деталь P1, и  определить, какой процент составляет  суммарный вес  поставок  поставщиками из каждого города   от общего веса поставленных  деталей P1.  Вывести  город, суммарный  вес  поставок  поставщиков из этого города, общий вес  поставленных деталей  P1, процент.');

insert into exercise (id, problem_statement, category_id, variant_number_lr, IS_PROTECTION, BD, IS_SORT, right_result)
values (2, 'Найти изделия  такие, что детали для изделия поставлялись из того же города, где находится поставщик, выполнивший поставку. Вывести полную информацию об изделиях: номер, название, город. ', 1, 2, 0, 1, 0, '[{"n_izd":"J2    "," category_name":"Перфоратор          ","town":"Рим                 "},{"n_izd":"J4    "," category_name":"Принтер             ","town":"Афины               "},{"n_izd":"J1    "," category_name":"Жесткий диск        ","town":"Париж               "},{"n_izd":"J7    "," category_name":"Лента               ","town":"Лондон              "},{"n_izd":"J3    "," category_name":"Считыватель         ","town":"Афины               "}]');

insert into exercise (id, problem_statement, category_id, variant_number_lr, IS_PROTECTION, BD, IS_SORT, right_result)
values (5, 'Найти изделия, для которых детали зеленого цвета   поставлялись поставщиком, который проживает в городе  с буквой  "а" в названии.  Вывести полную информацию об изделиях: номер, название, город.', 1, 5, 0, 1, 0, '[{"n_izd":"J2    "," category_name":"Перфоратор          ","town":"Рим                 "},{"n_izd":"J4    "," category_name":"Принтер             ","town":"Афины               "}]');

insert into exercise (id, problem_statement, category_id, variant_number_lr, IS_PROTECTION, BD, IS_SORT, right_result)
values (8, 'Вывести о каждой поставке информацию: 
•	город, откуда сделана поставка, 
•	город, куда поставлены детали,
•	количество деталей,
•	вес детали.
Упорядочить  по  городу,  откуда сделана  поставка,  и  количеству деталей.

', null, null, 0, 1, 1, '[{"townp":"Лондон              ","townj":"Афины               ","kol":100,"ves":12},{"townp":"Лондон              ","townj":"Париж               ","kol":200,"ves":12},{"townp":"Лондон              ","townj":"Рим                 ","kol":200,"ves":19},{"townp":"Лондон              ","townj":"Афины               ","kol":300,"ves":19},{"townp":"Лондон              ","townj":"Лондон              ","kol":300,"ves":19},{"townp":"Лондон              ","townj":"Афины               ","kol":500,"ves":19},{"townp":"Лондон              ","townj":"Рим                 ","kol":500,"ves":14},{"townp":"Лондон              ","townj":"Афины               ","kol":700,"ves":12},{"townp":"Лондон              ","townj":"Афины               ","kol":800,"ves":14},{"townp":"Париж               ","townj":"Рим                 ","kol":100,"ves":12},{"townp":"Париж               ","townj":"Лондон              ","kol":100,"ves":12},{"townp":"Париж               ","townj":"Афины               ","kol":100,"ves":17},{"townp":"Париж               ","townj":"Рим                 ","kol":200,"ves":17},{"townp":"Париж               ","townj":"Афины               ","kol":400,"ves":12},{"townp":"Париж               ","townj":"Лондон              ","kol":500,"ves":12},{"townp":"Рим                 ","townj":"Афины               ","kol":200,"ves":17},{"townp":"Рим                 ","townj":"Париж               ","kol":200,"ves":17},{"townp":"Рим                 ","townj":"Рим                 ","kol":200,"ves":17},{"townp":"Рим                 ","townj":"Афины               ","kol":200,"ves":17},{"townp":"Рим                 ","townj":"Осло                ","kol":400,"ves":17},{"townp":"Рим                 ","townj":"Париж               ","kol":400,"ves":17},{"townp":"Рим                 ","townj":"Афины               ","kol":500,"ves":17},{"townp":"Рим                 ","townj":"Лондон              ","kol":600,"ves":17},{"townp":"Рим                 ","townj":"Лондон              ","kol":800,"ves":17}]');

insert into exercise (id, problem_statement, category_id, variant_number_lr, IS_PROTECTION, BD, IS_SORT, right_result)
values (11, 'Найти поставщиков, поставлявших детали из того же города, где находится сам поставщик. Вывести полную информацию о поставщиках: номер, фамилия, город, рейтинг.', 1, null, 0, 1, 0, '[{"n_post":"S1    "," category_name":"Смит                ","reiting":20,"town":"Лондон              "},{"n_post":"S4    "," category_name":"Кларк               ","reiting":20,"town":"Лондон              "},{"n_post":"S2    "," category_name":"Джонс               ","reiting":10,"town":"Париж               "}]');

insert into exercise (id, problem_statement, category_id, variant_number_lr, IS_PROTECTION, BD, IS_SORT, right_result)
values (13, 'Найти поставки с объемом от 300 до 500 деталей. Вывести  фамилию поставщика, название изделия, название детали и количество деталей.', 1, null, 1, 1, 0, '[{" category_name_s":"Джонс               "," category_name_p":"Винт                "," category_name_j":"Жесткий диск        ","kol":400},{" category_name_s":"Джонс               "," category_name_p":"Винт                "," category_name_j":"Принтер             ","kol":500},{" category_name_s":"Джонс               "," category_name_p":"Винт                "," category_name_j":"Терминал            ","kol":400},{" category_name_s":"Блейк               "," category_name_p":"Винт                "," category_name_j":"Перфоратор          ","kol":500},{" category_name_s":"Кларк               "," category_name_p":"Блюм                "," category_name_j":"Считыватель         ","kol":300},{" category_name_s":"Кларк               "," category_name_p":"Блюм                "," category_name_j":"Лента               ","kol":300},{" category_name_s":"Адамс               "," category_name_p":"Кулачок             "," category_name_j":"Флоппи-диск         ","kol":500},{" category_name_s":"Адамс               "," category_name_p":"Кулачок             "," category_name_j":"Принтер             ","kol":400},{" category_name_s":"Адамс               "," category_name_p":"Блюм                "," category_name_j":"Принтер             ","kol":500}]');

insert into exercise (id, problem_statement, category_id, variant_number_lr, IS_PROTECTION, BD, IS_SORT, right_result)
values (15, 'Определить средний вес поставки  в города, где производят изделие с самым длинным названием. Округлить до двух знаков после запятой.', 2, null, 0, 1, 0, '[{"avg_ves":4200.00}]');

insert into exercise (id, problem_statement, category_id, variant_number_lr, IS_PROTECTION, BD, IS_SORT, right_result)
values (19, 'Выдать число  деталей, поставлявшихся для изделий,  у  которых  есть  поставки   с  весом  от 5000 до 6000.', 2, 3, 0, 1, 0, '[{"count_det":3}]');

insert into exercise (id, problem_statement, category_id, variant_number_lr, IS_PROTECTION, BD, IS_SORT, right_result)
values (23, 'Выдать число цветов деталей, поставлявшихся поставщиками, выполнявшими поставки для  изделий из Лондона.', 2, 7, 0, 1, 0, '[{"count_cvet":3}]');

insert into exercise (id, problem_statement, category_id, variant_number_lr, IS_PROTECTION, BD, IS_SORT, right_result)
values (26, 'Выдать  число  поставок деталей,  поставлявшихся  в города, где собирают  изделия,  в названии которых нет  буквы «о».', 2, null, 1, 1, 0, '[{"count_p":19}]');

insert into exercise (id, problem_statement, category_id, variant_number_lr, IS_PROTECTION, BD, IS_SORT, right_result)
values (29, 'Выдать  число   цветов  деталей,   поставлявшихся    в Рим  поставщиками, в имени которых   нет  буквы «р».', 2, null, 0, 1, 0, '[{"count_cvet":3}]');

insert into exercise (id, problem_statement, category_id, variant_number_lr, IS_PROTECTION, BD, IS_SORT, right_result)
values (30, 'Выдать  число   городов,  где проживают поставщики, поставлявшие детали   весом  менее  13.', 2, null, 1, 1, 0, '[{"count_town":3}]');



insert into alternative_sets_of_tables (id, TEXTSQL, id_exercise, right_result)
values (2, 'insert into spj  select * from  spj', 2, '[{"n_izd":"J1    "," category_name":"Жесткий диск        ","town":"Париж               "},{"n_izd":"J2    "," category_name":"Перфоратор          ","town":"Рим                 "},{"n_izd":"J3    "," category_name":"Считыватель         ","town":"Афины               "},{"n_izd":"J4    "," category_name":"Принтер             ","town":"Афины               "},{"n_izd":"J7    "," category_name":"Лента               ","town":"Лондон              "}]');

insert into alternative_sets_of_tables (id, TEXTSQL, id_exercise, right_result)
values (16, 'INSERT INTO s  VALUES(''S6'',''Петров'',30,''Москва'');
INSERT INTO spj VALUES  (''S6'',''P6'',''J5'',300);
update p set town=''Москва'' where n_det=''P6'';', 2, '[{"n_izd":"J2    "," category_name":"Перфоратор          ","town":"Рим                 "},{"n_izd":"J5    "," category_name":"Флоппи-диск         ","town":"Лондон              "},{"n_izd":"J4    "," category_name":"Принтер             ","town":"Афины               "},{"n_izd":"J1    "," category_name":"Жесткий диск        ","town":"Париж               "}]');

insert into alternative_sets_of_tables (id, TEXTSQL, id_exercise, right_result)
values (5, 'insert into spj  select * from  spj', 5, '[{"n_izd":"J2    "," category_name":"Перфоратор          ","town":"Рим                 "},{"n_izd":"J4    "," category_name":"Принтер             ","town":"Афины               "}]');

insert into alternative_sets_of_tables (id, TEXTSQL, id_exercise, right_result)
values (17, 'INSERT INTO s  VALUES(''S6'',''Петров'',30,''Москва'');
INSERT INTO spj VALUES  (''S6'',''P6'',''J5'',300);
update p set cvet=''Зеленый'' where n_det=''P6'';', 5, '[{"n_izd":"J2    "," category_name":"Перфоратор          ","town":"Рим                 "},{"n_izd":"J4    "," category_name":"Принтер             ","town":"Афины               "},{"n_izd":"J5    "," category_name":"Флоппи-диск         ","town":"Лондон              "}]');

insert into alternative_sets_of_tables (id, TEXTSQL, id_exercise, right_result)
values (18, 'INSERT INTO s  VALUES(''S6'',''Петров'',30,''Москва'');
INSERT INTO spj VALUES  (''S6'',''P6'',''J5'',300);
delete from spj where kol>300;', 8, '[{"townp":"Лондон              ","townj":"Афины               ","kol":100,"ves":12},{"townp":"Лондон              ","townj":"Рим                 ","kol":200,"ves":19},{"townp":"Лондон              ","townj":"Париж               ","kol":200,"ves":12},{"townp":"Лондон              ","townj":"Лондон              ","kol":300,"ves":19},{"townp":"Лондон              ","townj":"Афины               ","kol":300,"ves":19},{"townp":"Лондон              ","townj":"Лондон              ","kol":300,"ves":19},{"townp":"Париж               ","townj":"Рим                 ","kol":100,"ves":12},{"townp":"Париж               ","townj":"Афины               ","kol":100,"ves":17},{"townp":"Париж               ","townj":"Лондон              ","kol":100,"ves":12},{"townp":"Париж               ","townj":"Рим                 ","kol":200,"ves":17},{"townp":"Рим                 ","townj":"Афины               ","kol":200,"ves":17},{"townp":"Рим                 ","townj":"Афины               ","kol":200,"ves":17},{"townp":"Рим                 ","townj":"Париж               ","kol":200,"ves":17},{"townp":"Рим                 ","townj":"Рим                 ","kol":200,"ves":17}]');

insert into alternative_sets_of_tables (id, TEXTSQL, id_exercise, right_result)
values (11, 'insert into spj  select * from  spj;', 11, '[{"n_post":"S1    "," category_name":"Смит                ","reiting":20,"town":"Лондон              "},{"n_post":"S4    "," category_name":"Кларк               ","reiting":20,"town":"Лондон              "},{"n_post":"S2    "," category_name":"Джонс               ","reiting":10,"town":"Париж               "}]');

insert into alternative_sets_of_tables (id, TEXTSQL, id_exercise, right_result)
values (19, 'INSERT INTO s  VALUES(''S6'',''Петров'',30,''Москва'');
INSERT INTO spj VALUES  (''S6'',''P6'',''J5'',300);
update p set town=''Москва'' where n_det=''P6'';', 11, '[{"n_post":"S1    "," category_name":"Смит                ","reiting":20,"town":"Лондон              "},{"n_post":"S6    "," category_name":"Петров              ","reiting":30,"town":"Москва              "},{"n_post":"S2    "," category_name":"Джонс               ","reiting":10,"town":"Париж               "}]');

insert into alternative_sets_of_tables (id, TEXTSQL, id_exercise, right_result)
values (13, 'insert into spj  select * from  spj;', 13, '[{" category_name_s":"Джонс               "," category_name_p":"Винт                "," category_name_j":"Жесткий диск        ","kol":400},{" category_name_s":"Джонс               "," category_name_p":"Винт                "," category_name_j":"Принтер             ","kol":500},{" category_name_s":"Джонс               "," category_name_p":"Винт                "," category_name_j":"Терминал            ","kol":400},{" category_name_s":"Блейк               "," category_name_p":"Винт                "," category_name_j":"Перфоратор          ","kol":500},{" category_name_s":"Кларк               "," category_name_p":"Блюм                "," category_name_j":"Считыватель         ","kol":300},{" category_name_s":"Кларк               "," category_name_p":"Блюм                "," category_name_j":"Лента               ","kol":300},{" category_name_s":"Адамс               "," category_name_p":"Кулачок             "," category_name_j":"Флоппи-диск         ","kol":500},{" category_name_s":"Адамс               "," category_name_p":"Кулачок             "," category_name_j":"Принтер             ","kol":400},{" category_name_s":"Адамс               "," category_name_p":"Блюм                "," category_name_j":"Принтер             ","kol":500},{" category_name_s":"Джонс               "," category_name_p":"Винт                "," category_name_j":"Жесткий диск        ","kol":400},{" category_name_s":"Джонс               "," category_name_p":"Винт                "," category_name_j":"Принтер             ","kol":500},{" category_name_s":"Джонс               "," category_name_p":"Винт                "," category_name_j":"Терминал            ","kol":400},{" category_name_s":"Блейк               "," category_name_p":"Винт                "," category_name_j":"Перфоратор          ","kol":500},{" category_name_s":"Кларк               "," category_name_p":"Блюм                "," category_name_j":"Считыватель         ","kol":300},{" category_name_s":"Кларк               "," category_name_p":"Блюм                "," category_name_j":"Лента               ","kol":300},{" category_name_s":"Адамс               "," category_name_p":"Кулачок             "," category_name_j":"Флоппи-диск         ","kol":500},{" category_name_s":"Адамс               "," category_name_p":"Кулачок             "," category_name_j":"Принтер             ","kol":400},{" category_name_s":"Адамс               "," category_name_p":"Блюм                "," category_name_j":"Принтер             ","kol":500}]');

insert into alternative_sets_of_tables (id, TEXTSQL, id_exercise, right_result)
values (20, 'INSERT INTO s  VALUES(''S6'',''Петров'',30,''Москва'');
INSERT INTO spj VALUES  (''S6'',''P6'',''J5'',300);
delete from spj where kol>300;', 13, '[{" category_name_s":"Кларк               "," category_name_p":"Блюм                "," category_name_j":"Считыватель         ","kol":300},{" category_name_s":"Кларк               "," category_name_p":"Блюм                "," category_name_j":"Лента               ","kol":300},{" category_name_s":"Петров              "," category_name_p":"Блюм                "," category_name_j":"Флоппи-диск         ","kol":300}]');

insert into alternative_sets_of_tables (id, TEXTSQL, id_exercise, right_result)
values (21, 'update j set  category_name=''Флоппи-диск1'';
', 15, '[{"avg_ves":5529.17}]');

insert into alternative_sets_of_tables (id, TEXTSQL, id_exercise, right_result)
values (22, 'update p set ves=10;', 15, '[{"avg_ves":2666.67}]');

insert into alternative_sets_of_tables (id, TEXTSQL, id_exercise, right_result)
values (23, 'update spj set kol=480 where n_det=''P5'';
', 19, '[{"count_det":6}]');

insert into alternative_sets_of_tables (id, TEXTSQL, id_exercise, right_result)
values (24, 'update spj set kol=480 where n_det=''P5'';
update spj set kol=100 where n_det in (''P2'',''P3'',''P4'',''P1'');
update p set ves=13 where n_det=''P5'';
delete from spj where n_det=''P3'';
', 19, '[{"count_det":2}]');

insert into alternative_sets_of_tables (id, TEXTSQL, id_exercise, right_result)
values (25, 'delete from spj where n_det=''P2'';', 23, '[{"count_cvet":2}]');

insert into alternative_sets_of_tables (id, TEXTSQL, id_exercise, right_result)
values (26, 'INSERT INTO p VALUES(''P7'',''Блюм2'',''Белый'',19,''Лондон'');
INSERT INTO p VALUES(''P8'',''Блюм3'',''Черный'',19,''Лондон'');
INSERT INTO spj VALUES  (''S5'',''P7'',''J4'',800);
INSERT INTO spj VALUES  (''S5'',''P8'',''J4'',400);
', 23, '[{"count_cvet":5}]');

insert into alternative_sets_of_tables (id, TEXTSQL, id_exercise, right_result)
values (27, 'update j set  category_name=''Ленточка'' where n_izd=''J7'';', 26, '[{"count_p":14}]');

insert into alternative_sets_of_tables (id, TEXTSQL, id_exercise, right_result)
values (28, 'update j set  category_name=''Диск'' where n_izd=''J2'';
update j set  category_name=''Ленточка'' where n_izd<>''J2'';', 26, '[{"count_p":5}]');

insert into alternative_sets_of_tables (id, TEXTSQL, id_exercise, right_result)
values (29, 'update s set  category_name=''Райли'' where n_post=''S2'';', 29, '[{"count_cvet":2}]');

insert into alternative_sets_of_tables (id, TEXTSQL, id_exercise, right_result)
values (30, 'INSERT INTO p VALUES(''P7'',''Блюм2'',''Белый'',19,''Лондон'');
INSERT INTO p VALUES(''P8'',''Блюм3'',''Черный'',19,''Лондон'');
INSERT INTO spj VALUES  (''S5'',''P7'',''J2'',800);
INSERT INTO spj VALUES  (''S5'',''P8'',''J2'',400);
', 29, '[{"count_cvet":5}]');

insert into alternative_sets_of_tables (id, TEXTSQL, id_exercise, right_result)
values (31, 'update s set town =''Лондон'' where n_post=''S2'';', 30, '[{"count_town":2}]');

insert into alternative_sets_of_tables (id, TEXTSQL, id_exercise, right_result)
values (32, 'INSERT INTO p VALUES(''P7'',''Блюм2'',''Белый'',10,''Лондон'');
INSERT INTO spj VALUES  (''S4'',''P7'',''J2'',800);
update s set town =''Москва'' where n_post=''S4'';', 30, '[{"count_town":4}]');