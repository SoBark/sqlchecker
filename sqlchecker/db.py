# import sqlite3
import psycopg2
import os
#import logging

import click
from flask import current_app, g
from flask.cli import with_appcontext


# def get_db():
#     if 'db' not in g:
#         g.db = sqlite3.connect(
#             current_app.config['DATABASE'],
#             detect_types=sqlite3.PARSE_DECLTYPES
#         )
#         g.db.row_factory = sqlite3.Row

#     return g.db

def get_db():
    if 'db' not in g:
        g.db = psycopg2.connect(
            host='localhost',
            database="flask_db",
            # user=os.environ['DB_USERNAME'],
            # password=os.environ['DB_PASSWORD'],
            user=current_app.config['DB_USERNAME'],
            password=current_app.config['DB_PASSWORD'],
        )
    return g.db


def close_db(exception=None):
    db = g.pop('db', None)

    if db is not None:
        db.close()



def init_db():
    db = get_db()
    cur = db.cursor()
    with current_app.open_resource('schema.sql') as f:
        cur.execute(f.read().decode('utf8'))
    db.commit()
    close_db()

@click.command('init-db')
@with_appcontext
def init_db_command():
    """Clear the existing data and create new tables."""
    init_db()
    click.echo('Initialized the questions database.')


def init_app(app):
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)


def select_query_dict(connection, query, data=[]):
    """
    Run generic select query on db, returns a list of dictionaries
    """
    #logger.debug('Running query: {}'.format(query))

    # Open a cursor to perform database operations
    cursor = connection.cursor()
    #logging.debug('Db connection succesful')

    # execute the query
    try:
        #logger.info('Running query.')
        if len(data):
            cursor.execute(query, data)
        else:
            cursor.execute(query)
        columns = list(cursor.description)
        result = cursor.fetchall()
        #logging.debug('Query executed succesfully')
    except (Exception, psycopg2.DatabaseError) as e:
        #logging.error(e)
        print(e)
        cursor.close()
        exit(1)

    cursor.close()

    # make dict
    results = []
    for row in result:
        row_dict = {}
        for i, col in enumerate(columns):
            row_dict[col.name] = row[i]
        results.append(row_dict)

    return results