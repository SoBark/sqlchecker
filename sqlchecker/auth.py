from calendar import c
import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash, generate_password_hash

from sqlchecker.db import get_db, select_query_dict
import requests

bp = Blueprint('auth', __name__, url_prefix='/auth')

@bp.route('/register', methods=('GET', 'POST'))
def register():
    if request.method == 'POST':
        email = request.form['email']
        username = request.form['username']
        password = request.form['password']
        db = get_db()
        error = None

        if not email:
            error = 'Email is required'
        elif not username:
            error = 'Username is required.'
        elif not password:
            error = 'Password is required.'

        if error is None:
            try:
                cursor = db.cursor()
                cursor.execute(
                    "INSERT INTO users (email, username, password, role) VALUES (%s ,%s, %s, %s)",
                    (email, username, generate_password_hash(password), 'student'),
                )
                db.commit()
                cursor.close()
            except db.IntegrityError:
                error = f"User {username} is already registered."
            else:
                return redirect(url_for("auth.login"))

        flash(error)

    return render_template('auth/register.html')


@bp.route('/login', methods=('GET', 'POST'))
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        db = get_db()
        error = None

        user = select_query_dict(db, 'SELECT * FROM users WHERE username = %s', [username] )[0]
        if user is None:
            error = 'Incorrect username.'
        elif not check_password_hash(user['password'], password):
            error = 'Incorrect password.'

        if error is None:
            session.clear()
            session['user_id'] = user['id']
            return redirect(url_for('test.work_mode'))

        flash(error)

    return render_template('auth/login.html')



@bp.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))


def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        
        t = request.headers['token']
        
        u = requests.get('https://api.ciu.nstu.ru/v1.1/student/check_dispace_token/'+t, headers={'X-Apikey': 'FB8EEED25F6150E3E0530718000A3425'})
        ujson = u.json()
        if ujson['res']  == 'not found':
            return redirect(url_for('test.work_mode'))
        session['user_id'] = ujson['res']['ID_CIU_UNIQUE']
        return view(**kwargs)

    return wrapped_view


@bp.before_app_request
def load_logged_in_user():
    user_id = session.get('user_id')
    
    if user_id is None:
        g.user = None
    else:
        g.user = select_query_dict(get_db(), 'SELECT * FROM student WHERE id = %s', [user_id])[0]
        
        

