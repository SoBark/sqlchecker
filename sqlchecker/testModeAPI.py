from pickle import NONE
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)

from sqlchecker.db import get_db, select_query_dict

from random import randint, shuffle, choice

from sqlchecker.test import write_solution_attempt

bp = Blueprint('test', __name__)

#ready
@bp.route('/wmode/test',methods=('get',))
def test_get():
    return render_template('work_mode/test.html')

@bp.route('/wmode/test/start',methods=('GET',))
def test_start_get_drow():
    flag = 0
    exercise = test_start_get(flag)

    if flag == 0:
        if len(exercise) != 0:
            flash("вы уже отправили решение")
            return render_template('work_mode/test.html')
        else:
            return render_template('work_mode/test_start.html', exercise = exercise[0])
    if flag == 1:
        return render_template('work_mode/have_unsolved_problems_test.html')


@bp.route('/wmode/test/continue',methods=('GET',))
def test_continue_get_drow():   
    exercise = test_continue_get()  
    if len(exercise) != 0:
        flash("вы уже отправили решение")
        return render_template('work_mode/test.html')
    if len(exercise) == 0:
        flash("вы еще не приступали к заданиям")
        return render_template('work_mode/test.html')
    return render_template('work_mode/test_continue.html', exercise = exercise[0])

@bp.route('/wmode/test/start',methods=('POST',))
@bp.route('/wmode/test/continue',methods=('POST',))
def test_start_and_continue_post_drow():
    flag = 0
    error = None
    number_of_ex_to_continue = test_start_and_continue_post(flag, error)
    if flag == 0:
        flash(error)
        return render_template('work_mode/test_continue.html', exercise = number_of_ex_to_continue[0])
    return render_template('work_mode/test.html')

def test_start_get(flag):
    db = get_db()

    exercise = select_query_dict(db,                                                                 
        'select exercise.* '
        'from assignment '
        'left join solution_attempts on assignment.id = solution_attempts.id_assignment '
        'join student on student.id = assignment.id_student '
        'join users on users.id = student.id_user '
        'join exercise on exercise.id = assignment.id_exercise '
        'join category on category.id = exercise.category_id '
        'where solution_attempts.id_assignment is not null and assignment.usage_mode = 2 and users.id = %s ', [g.user['id']])
    if len(exercise) != 0:
        flag = 0
        return exercise
        
    id_assignment = select_query_dict(db,                                                                 
        'select assignment.id '
        'from assignment '
        'join student on student.id = assignment.id_student '
        'join users on users.id = student.id_user '
        'left join solution_attempts on assignment.id = solution_attempts.id_assignment '
        'where solution_attempts.id_assignment is null and assignment.usage_mode = 2 and users.id = %s ', [g.user['id']])
    
    
    if(len(id_assignment)):
        flag = 1
        return id_assignment
    
    CURRENT_DATE = select_query_dict(db,'select now()', NONE)[0]['now'] #дата решения
    CURRENT_TIME = select_query_dict(db,'select now()', NONE)[0]['now'] #дата решения
    cursor = db.cursor()
    cursor.execute(
        'INSERT INTO assignment (id_student, solving_data, time_start, id_exercise, usage_mode)'
        ' VALUES (%s, %s, %s, %s, %s) '
        'returning id',
        (g.user['id'], CURRENT_DATE,CURRENT_TIME, int(choice(select_query_dict(db, 'select distinct(exercise.id) '
                                                                                   'from exercise '
                                                                                   'join category on category.id = exercise.category_id '
                                                                                   'where category.id = 99 '
                                                                                   'except  '
                                                                                   'select assignment.id_exercise  '
                                                                                   'from assignment '
                                                                                   'join student on student.id = assignment.id_student '
                                                                                   'join users on users.id = student.id_user '
                                                                                   'where usage_mode = 2 and student.grp = %s ', ['pmi91']))['id']), 2))
    id_assignment = cursor.fetchone()[0]
    db.commit()
    cursor.close()
    exercise = select_query_dict(db,                                                                 
        'select exercise.* '
        'from exercise join assignment on exercise.id = assignment.id_exercise where assignment.id = %s ', [id_assignment])
    flag = 0
    return exercise 

def test_continue_get():
    db = get_db()
    exercise = select_query_dict(db,                                                                 
        'select exercise.* '
        'from assignment '
        'left join solution_attempts on assignment.id = solution_attempts.id_assignment '
        'join student on student.id = assignment.id_student '
        'join users on users.id = student.id_user '
        'join exercise on exercise.id = assignment.id_exercise '
        'join category on category.id = exercise.category_id '
        'where solution_attempts.id_assignment is not null and assignment.usage_mode = 2 and users.id = %s ', [g.user['id']])
    if len(exercise) != 0:
        return exercise

    exercise = select_query_dict(db,                                                                 
        'select exercise.* '
        'from assignment '
        'left join solution_attempts on assignment.id = solution_attempts.id_assignment '
        'join student on student.id = assignment.id_student '
        'join users on users.id = student.id_user '
        'join exercise on exercise.id = assignment.id_exercise '
        'join category on category.id = exercise.category_id '
        'where solution_attempts.id_assignment is null and assignment.usage_mode = 2 and users.id = %s ', [g.user['id']])
    if len(exercise) == 0:
        return exercise
    
    return exercise

def test_start_and_continue_post(flag, error):
    db = get_db()

    body = request.form['body']

    error = None
    id_assignment = select_query_dict(db,                                                                 
        'select assignment.id '
        'from assignment '
        'join student on student.id = assignment.id_student '
        'join users on users.id = student.id_user '
        'left join solution_attempts on assignment.id = solution_attempts.id_assignment '
        'where  solution_attempts.id_assignment is null and assignment.usage_mode = 2 and users.id = %s ', [g.user['id']])

    if not body:
        error = 'Вы не записали ответ'
        number_of_ex_to_continue = select_query_dict(db,                                                                 
        'select exercise.* '
        'from assignment '
        'join exercise on exercise.id = assignment.id_exercise '
        'where assignment.id = %s ',[int(id_assignment[0]['id'])])
    if error is not None:
        flag = 0
        return number_of_ex_to_continue[0]
    else:
        write_solution_attempt(int(id_assignment[0]['id']), body, 0)
        flag = 1
        return 1
