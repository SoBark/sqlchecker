from crypt import methods
import json
from pickle import NONE
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from psycopg2 import connect
from werkzeug.exceptions import abort

from sqlchecker.auth import login_required
from sqlchecker.db import get_db, select_query_dict

from sqlchecker.message_broker import get_msg_queue

from random import randint, shuffle, choice
import datetime
import requests

bp = Blueprint('test', __name__)

@bp.after_request
def add_security_headers(resp):
    
    if 'token' in request.args:
        resp.headers['token']= request.args['token']
    return resp

@bp.route('/', methods=('GET',))
def index_GET():
    db = get_db()
    questions = select_query_dict(db,
        'SELECT questions.id, questions.body'
        ' FROM questions', NONE)
    return render_template('test/index.html', questions=questions)


@bp.route('/', methods=('POST',))
@login_required
def index_POST():
    #title = request.form['title']
    body = request.form['body']
    questionId = request.form['questionId']
    error = None

    if not body:
        error = 'Answer is required.'
    if not questionId:
        error = 'Question error'
    if error is not None:
        flash(error)
    else:
        db = get_db()
        cursor = db.cursor()
        cursor.execute(
            'INSERT INTO answers (body, question_id, usr_id, check_status)'
            ' VALUES (%s, %s, %s, %s)',
            (body, questionId, g.user['id'], 0)
        )
        db.commit()
        cursor.close()
        #add task to queue
        msg_queue = get_msg_queue()
        channel = msg_queue.channel()
        channel.queue_declare(queue='SQLChecker')
        body = {'usr_id':g.user['id'],'question_id':questionId }
        channel.basic_publish(exchange='',
                      routing_key='SQLChecker',
                      body=json.dumps(body))
        
        flash('Answer written')
        #return redirect(url_for('blog.index'))
    db = get_db()
    questions = select_query_dict(db,
        'SELECT id, body'
        ' FROM questions', NONE)
    return render_template('test/index.html', questions=questions)




def get_answer_last(question_id, usr_id, check_author=True):
    #print(f'shaitan {question_id} user {usr_id}')
    answer = select_query_dict(get_db(), 
        'SELECT answers.id, answers.question_id, answers.usr_id,'
        ' answers.body, answers.created, answers.result'
        ' FROM answers'
        ' WHERE answers.question_id = %s AND'
        '       answers.usr_id = %s'
        ' ORDER BY answers.created DESC',
         [question_id, usr_id ])
    if (answer == []):
        answer = NONE
    else:
        answer = answer[0]
    # if post is None:
    #     abort(404, f"Post id {id} doesn't exist.")
    #print(f'hate')
    if answer != NONE and check_author and answer['usr_id'] != g.user['id']:
        abort(403)

    return answer


@bp.route('/<int:question_id>/is_checked', methods=('GET',))
@login_required
def is_checked(question_id):
    #print(f'zag u {question_id} user {g.user["username"]}')
    answer = get_answer_last(question_id, g.user['id'])
    #print('Nice answer body!')

    return render_template('test/isChecked.html', answer=answer)


@bp.route('/<int:lesson_id>/page_of_lesson', methods=('GET','POST'))
def page_of_lesson(lesson_id):
    print(request.args.headers)
    db = get_db()
    data = select_query_dict(db,
        'SELECT topic_description from topics where id = %s', [lesson_id])[0]   
    return render_template('test/page_of_lesson.html', data = data, lesson_id = lesson_id)


@bp.route('/wmode/self_learning',methods=('GET',))
@login_required
def self_learning():
    db = get_db()
    data = select_query_dict(db,
        'SELECT * from topics where id = %s', request.args['id_praktiki'])  
    return render_template('work_mode/self_learning.html', data = data[0], id_praktiki = int(request.args['id_praktiki']), token = request.args['token'])



@bp.route('/wmode/training/start',methods=('GET',))
def self_learning_start_get():#кнопка начать.  
    
    id_assignment = 0

    id_praktiki = int(request.args['id_praktiki']) #получение номера урока из параметров запроса
    db = get_db() # обращение к базе данных
    
    id_of_ex_to_continue = select_query_dict(db,                                                                 
        ' select exercise.id  '
        ' from assignment  '
        ' join exercise on exercise.id = assignment.id_exercise  '
        ' left join solution_attempts on assignment.id = solution_attempts.id_assignment  '
        ' join category on category.id = exercise.category_id  '
        ' where assignment.id_student = %s and assignment.usage_mode = 0 and category.id = %s '
        ' except '
        ' (select exercise.id '
        ' from assignment  '
        ' join exercise on exercise.id = assignment.id_exercise  '
        ' left join solution_attempts on assignment.id = solution_attempts.id_assignment  '
        ' join category on category.id = exercise.category_id  '
        ' where (solution_attempts.result = 0 and assignment.id_student = %s and assignment.usage_mode = 0 and category.id = %s)) '
        ,[g.user['id'], (id_praktiki), g.user['id'], (id_praktiki)]) #выбор заданий, которые не дорешил студент


    if len(id_of_ex_to_continue) > 0 : # если недорешенных заданий нет, то сообщаем студенту об этом
        return render_template('work_mode/have_unsolved_problems.html', id_praktiki = int(request.args['id_praktiki']), token = request.args['token'])
    
    
    #запрос на проверку количество решенных задач учеником, если их меньше 2, то он не получает 
    #для 1 урока такой проверки не происходит.
    if id_praktiki == 1:
        
        number_of_exes = select_query_dict(db,
            'SELECT exercise.id from exercise where exercise.category_id = %s except (select assignment.id_exercise from assignment '
            'where usage_mode = 0 and assignment.id_student = %s)', [id_praktiki, g.user['id']])
        if(len(number_of_exes) == 0):
            flash('Вы решили все доступные задания')
            data = select_query_dict(db,
                'SELECT * from topics where id = %s', request.args['id_praktiki'])
            return render_template('work_mode/self_learning.html', data = data[0], id_praktiki = int(request.args['id_praktiki']), token = request.args['token'])

        choice_of_ex = choice(number_of_exes)['id']

        CURRENT_DATE = select_query_dict(db,'select now()', NONE)[0]['now'] #дата решения
        CURRENT_TIME = select_query_dict(db,'select now()', NONE)[0]['now'] #дата решения
        cursor = db.cursor()
        cursor.execute(
            'INSERT INTO assignment (id_student, solving_data, time_start, id_exercise, usage_mode)'
            ' VALUES (%s, %s, %s, %s, %s) '
            'returning id',
            (g.user['id'], CURRENT_DATE,CURRENT_TIME, choice_of_ex, 0)
        )# запись в таблицу заданий
        id_assignment = cursor.fetchone()[0]
        db.commit()
        cursor.close()

        exercise = select_query_dict(db,
            'SELECT * from exercise where id = %s and exercise.category_id = %s', [choice_of_ex, id_praktiki])[0]

    else: 
        id_user = g.user['id']
    
        number_of_resolved_ex = select_query_dict(db,         # запрос на количество решенных задач по предыдущей теме.(минимум 2)
        'select count(solution_attempts.id) '
        'from solution_attempts '
        'join assignment on assignment.id = solution_attempts.id_assignment '
        'join exercise on exercise.id = assignment.id_exercise '
        'join category on category.id = exercise.category_id '
        'where (solution_attempts.result = 0 and assignment.id_student = %s and category.id = %s and assignment.usage_mode = 0)',[id_user, id_praktiki-1])[0]
        

        if number_of_resolved_ex['count'] >= 2: # если решенных задач 2 или больше то студенту выдается запрос
            number_of_exes = select_query_dict(db,
                'SELECT exercise.id from exercise where exercise.category_id = %s', [id_praktiki])
            choice_of_ex = choice(number_of_exes)['id']

            CURRENT_DATE = select_query_dict(db,'select now()', NONE)[0]['now'] #дата решения
            CURRENT_TIME = select_query_dict(db,'select now()', NONE)[0]['now'] #дата решения
            cursor = db.cursor()
            cursor.execute(
                'INSERT INTO assignment (id_student, solving_data, time_start, id_exercise, usage_mode)'
                ' VALUES (%s, %s, %s, %s, %s) '
                'returning id',
                (g.user['id'], CURRENT_DATE,CURRENT_TIME, choice_of_ex, 0)
            )
            id_assignment = cursor.fetchone()[0]
            db.commit()
            cursor.close()

            exercise = select_query_dict(db,
                'SELECT * from exercise where id = %s and exercise.category_id = %s', [choice_of_ex, id_praktiki])[0]
        else:#если решенных задач меньше 2 то об этом сообщается студенту
            return render_template('work_mode/not_unavailable_for_training.html', id_praktiki = int(request.args['id_praktiki']), token = request.args['token'])

    return render_template('work_mode/training.html', exercise = exercise, id_assignment = id_assignment,  id_praktiki = id_praktiki, token = request.args['token'])


@bp.route('/wmode/training/start',methods=('POST',))
@bp.route('/wmode/training/continue',methods=('POST',))
def self_learning_start_and_continue_post():
    db = get_db() #обращение к бд
    
    id_praktiki = (request.args['id_praktiki']) #номер урока из параметров запроса
    body = request.form['body'] #текст, введенный студентом

    error = None

    if not body:#проверки ответа на пустоту
        error = 'Вы не ввели ответ'
        number_of_ex_to_continue = select_query_dict(db,                                                                 
        'select exercise.* '
        'from assignment '
        'join exercise on exercise.id = assignment.id_exercise '
        'where assignment.id = %s ',[request.form['id_assignment']])
 
    if error is not None:
        flash(error)
        return render_template('work_mode/training.html', exercise = number_of_ex_to_continue[0], id_assignment = request.form['id_assignment'], token = request.args['token'])
    else:
        #если ответ записан, запись делается в таблицу с попытками решений 
        id_ass = request.form['id_assignment']
        
        write_solution_attempt(int(id_ass), body, 0)# сама запись в таблицу и постановка в очередь
        data = select_query_dict(db,
        'SELECT * from topics where id = %s', id_praktiki)
        return render_template('work_mode/self_learning.html', data = data[0], id_praktiki = int(request.args['id_praktiki']), token = request.args['token'])

    

@bp.route('/wmode/training/continue',methods=('GET',))
def self_learning_continue():#кнопка продолжить.

    
    id_praktiki = int(request.args['id_praktiki'])# номер урока
    id_assignment = 0
    db = get_db() #обращение е базе данных
    id_user = g.user['id'] # идентификатор студента


    id_of_ex_to_continue = select_query_dict(db,                                                                 
        ' select exercise.id  '
        ' from assignment  '
        ' join exercise on exercise.id = assignment.id_exercise  '
        ' left join solution_attempts on assignment.id = solution_attempts.id_assignment  '
        ' join category on category.id = exercise.category_id  '
        ' where assignment.id_student = %s and assignment.usage_mode = 0 and category.id = %s '
        ' except '
        ' (select exercise.id '
        ' from assignment  ' 
        ' join exercise on exercise.id = assignment.id_exercise  '
        ' left join solution_attempts on assignment.id = solution_attempts.id_assignment  '
        ' join category on category.id = exercise.category_id  '
        ' where (solution_attempts.result = 0 and assignment.id_student = %s and assignment.usage_mode = 0 and category.id = %s)) '
        ,[g.user['id'], (id_praktiki), g.user['id'], (id_praktiki)])# выборка всех недорешанных задач студента

    if len(id_of_ex_to_continue) != 0:#если есть недорешенное, то студент его получает
        number_of_ex_to_continue = select_query_dict(db, 'select exercise.* from exercise where id = %s', [id_of_ex_to_continue[0]['id']])
    else:
        number_of_ex_to_continue = []

    if len(number_of_ex_to_continue) == 0:#если недорешенных задачи нет, студенту выводится сообщение об этом
        flash("Вы еще не приступали к заданиям.")
        
        data = select_query_dict(db,
        'SELECT * from topics where id = %s', request.args['id_praktiki'])
        return render_template('work_mode/self_learning.html', data = data[0], id_praktiki = int(request.args['id_praktiki']), token = request.args['token'])

    id_assignment = select_query_dict(db,                                                                 
        'select assignment.id '
        'from assignment '
        'left join solution_attempts on assignment.id = solution_attempts.id_assignment '
        'where  assignment.usage_mode = 0 and assignment.id_student = %s '
        'except '
        'select assignment.id '
        'from assignment '
        'left join solution_attempts on assignment.id = solution_attempts.id_assignment '
        'where solution_attempts.result = 0 and assignment.usage_mode = 0 and assignment.id_student = %s'
        , [g.user['id'], g.user['id']])[0]
    
    return render_template('work_mode/training.html', exercise = number_of_ex_to_continue[0], id_assignment = id_assignment['id'],  id_praktiki = id_praktiki, token = request.args['token'])


@bp.route('/wmode',methods=('GET',))#4 режима работы: 1 - контрольная работа 2 - решение задачи выданной на защиту 3 - отладка задания которое было выдано в лаб. работе 4 - прохождение курса
def work_mode():
    return render_template('work_mode/work_mode.html')

# @bp.route('/wmode/defence',methods=('GET',))
# def defence():
#     return render_template('work_mode/defence.html')

@bp.route('/wmode/admin',methods=('GET',))
def admin_get():
    db = get_db()
    questions = select_query_dict(db,
        'SELECT student.id, student.id_user, student.fio, student.faculty, student.grp'
        ' FROM student', NONE)
    exercises = select_query_dict(db,
        'SELECT exercise.*'
        ' FROM exercise', NONE)

    return render_template('work_mode/admin.html', questions=questions, exercises = exercises) 

@bp.route('/wmode/admin', methods=['GET', 'POST'])
def admin_post():
    if (len(request.form.getlist('give_ex_to_dif')) == 1):#если нажали дать запрос на защиту
        if (request.form.getlist('give_ex_to_dif')[0] == "дать запрос на защиту"):  
            n_id_exercise = 1 #рандомное число из лабораторных работ//нужна помощь с осмыслением
            fio = request.form['fio']
            grp = request.form['grp']
            faculty = request.form['faculty']
            nlr = request.form['nlr']  
            db = get_db()  
            #user = select_query_dict(db, 'SELECT * FROM users WHERE username = %s', [username] )[0]
            id_student = select_query_dict(db,'select distinct student.id from student where fio = %s and faculty = %s and grp = %s',[fio,faculty,grp])[0] 
            db = get_db()
            n_id_exercise = select_query_dict(db,' select count(exercise.id) from exercise', NONE)[0]
            cursor = db.cursor()

            cursor.execute(
                'INSERT INTO ability_defence (id_student, id_exercise)'
                'VALUES (%s, %s)'
                ,
                (id_student['id'], randint(1,n_id_exercise['count'] ))
            )  
            db.commit()
            cursor.close()
            return render_template('work_mode/admin.html',id_student=id_student,n_id_exercise=n_id_exercise)

    if (len(request.form.getlist('give_ex_to_test')) == 1):#если назначить контрольную работу
        if (request.form.getlist('give_ex_to_test')[0] == "назначить контрольную работу"): 
            grp = request.form['grp']
            faculty = request.form['faculty']
            list_of_ids = []
            db = get_db() 
            list_of_test_exercises = select_query_dict(db,'select exercise.id from exercise join category on category.number_lr = exercise.number_primer_number_lr where category.id = 99',NONE)
      
            for dictionary in list_of_test_exercises:
                list_of_ids.append(dictionary['id'])
            shuffle(list_of_ids)
            nubmer_of_students = select_query_dict(db,'select student.id from student where grp = %s and faculty = %s',[grp,faculty])
            print(nubmer_of_students)
            for i in nubmer_of_students:
                cursor = db.cursor()
                cursor.execute(
                'INSERT INTO test_exercises (id_student, id_exercise)'
                ' VALUES (%s, %s)',
                (i['id'],list_of_ids[0])) 
                db.commit()
                cursor.close()
                del list_of_ids[0]
                print(list_of_ids) 

            return render_template('work_mode/admin.html')

    return render_template('work_mode/admin.html')


@bp.route('/wmode/defence/choose',methods=('GET','POST'))
def defence_choose_get():
    db = get_db()#обращение к бд
    if request.method == 'GET':#если метод обращения get
        id_assignment = select_query_dict(db,                                                                 
        'select assignment.id '
        'from assignment '
        'join student on student.id = assignment.id_student '
        'join users on users.id = student.id_user '
        'left join solution_attempts on assignment.id = solution_attempts.id_assignment '
        'where solution_attempts.id_assignment is null and assignment.usage_mode = 3 and users.id = %s ', [g.user['id']])
        if(len(id_assignment)):#проверка есть ли недорешанные задачи у стеднта
            return render_template('work_mode/have_unsolved_problems_defence.html')
        else:
            return render_template('work_mode/defence_choose.html')
    if request.method == 'POST':#если метод обращения post
        group = select_query_dict(db, 'select grp from student '
                                      'join users on users.id = student.id_user '
                                      'where users.id = %s  ', [g.user['id']])[0]['grp']#получаем группу студента
        number_lr = request.form['number_lr']#получаем номер введенный студентом
        exercise_ids = select_query_dict(db,                                                                 
        'select distinct(exercise.id) '
        'from exercise '
        'join category on category.id = exercise.category_id '
        'where category.id in(select category.id '
        'from category '
        'where category.number_lr = %s) and exercise.is_protection = 1 '
        'except '
        'select assignment.id_exercise '
        'from assignment '
        'join student on student.id = assignment.id_student '
        'join users on users.id = student.id_user '
        'where usage_mode = 3 and student.grp = %s', [int(number_lr), group])
        if (len(exercise_ids) == 0):
            flash("Нет такого номера лаботарной работы")#проверка на корректность ввода номера лабораторной работы
            return render_template('work_mode/defence_choose.html')
        else:
            return redirect(url_for('test.defence_start_get', number_lr = number_lr))
        

@bp.route('/wmode/defence/start',methods=('GET',))
def defence_start_get(): 
    db = get_db()#обращение к бд
    
    group = select_query_dict(db, 'select grp from student '
                                      'join users on users.id = student.id_user '
                                      'where users.id = %s ', [g.user['id']])[0]['grp']#выборка группы студента
    exercise_ids = select_query_dict(db,                                                                 
        'select distinct(exercise.id) '
        'from exercise '
        'join category on category.id = exercise.category_id '
        'where category.id in(select category.id '
        'from category '
        'where category.number_lr = %s) and exercise.is_protection = 1 '
        'except '
        'select assignment.id_exercise '
        'from assignment '
        'join student on student.id = assignment.id_student '
        'join users on users.id = student.id_user '
        'where usage_mode = 3 and student.grp = %s', [int(request.args['number_lr']),group])#выборка номеров задач, которые еще не выданы дургим студентам
    
    ids =[]
    for i in exercise_ids:
        ids.append(i['id'])
    choice_of_ex = choice(ids)
#выдоча запроса и запись в базу данных информации о выданном задании 
    CURRENT_DATE = select_query_dict(db,'select now()', NONE)[0]['now'] #дата решения
    CURRENT_TIME = select_query_dict(db,'select now()', NONE)[0]['now'] #дата решения
    cursor = db.cursor()
    cursor.execute(
        'INSERT INTO assignment (id_student, solving_data, time_start, id_exercise, usage_mode)'
        ' VALUES (%s, %s, %s, %s, %s) ',
        (g.user['id'], CURRENT_DATE,CURRENT_TIME, choice_of_ex, 3))
    db.commit()
    cursor.close()

    exercise = select_query_dict(db, 'select exercise.* from exercise where exercise.id = %s',[choice_of_ex])

    return render_template('work_mode/defence_start.html',  exercise = exercise[0])

@bp.route('/wmode/defence/continue',methods=('GET',))
def defence_continue_get():
    db = get_db()#обращение к бд
    group = select_query_dict(db, 'select grp from student '
                                      'join users on users.id = student.id_user '
                                      'where users.id = %s ', [g.user['id']])[0]['grp']
    exercise = select_query_dict(db,                                                                 
        'select exercise.* '
        'from assignment '
        'left join solution_attempts on assignment.id = solution_attempts.id_assignment '
        'join student on student.id = assignment.id_student '
        'join users on users.id = student.id_user '
        'join exercise on exercise.id = assignment.id_exercise '
        'join category on category.id = exercise.category_id '
        'where  solution_attempts.id_assignment is null and assignment.usage_mode = 3 and users.id = %s ', [g.user['id']])
    if len(exercise) == 0:
         flash("Вы еще не приступали к заданиям.")
         return render_template('work_mode/defence.html')
    return render_template('work_mode/defence_continue.html', exercise = exercise[0])

@bp.route('/wmode/defence/start',methods=('POST',))
@bp.route('/wmode/defence/continue',methods=('POST',))
def defence_start_and_continue_post():
    db = get_db()#обращение к бд

    body = request.form['body']#текст введенный студента

    error = None
    id_assignment = select_query_dict(db,                                                                 
        'select assignment.id '
        'from assignment '
        'join student on student.id = assignment.id_student '
        'join users on users.id = student.id_user '
        'left join solution_attempts on assignment.id = solution_attempts.id_assignment '
        'where  solution_attempts.id_assignment is null and assignment.usage_mode = 3 and users.id = %s ', [g.user['id']])#нахождение номера задания который решает студент
#блок проверки ответа на пустоту
    if not body:
        error = 'Вы не записали ответ'
        number_of_ex_to_continue = select_query_dict(db,                                                                 
        'select exercise.* '
        'from assignment '
        'join exercise on exercise.id = assignment.id_exercise '
        'where assignment.id = %s ',[int(id_assignment[0]['id'])])
    if error is not None:
        flash(error)
        return render_template('work_mode/defence_continue.html', exercise = number_of_ex_to_continue[0])
    else:#если не пустой, ответ записывается в бд и ставится в очередь на проверку.
            
        write_solution_attempt(int(id_assignment[0]['id']), body, 0)
        return render_template('work_mode/defence.html')

    return render_template('work_mode/defence.html')

# @bp.route('/wmode/defence/start',methods=('POST',))
# def defence_start_post(): #запись ответа студента
#     db = get_db()
#     body = request.form['body']
#     id_exercise = request.form['questionId']
#     error = None

#     if not body:
#         error = 'Answer is required.'
#     if not id_exercise:
#         error = 'Question error'
#     if error is not None:
#         flash(error)
#     else:
#         db = get_db()
#         CURRENT_DATE = select_query_dict(db,'select now()', NONE)[0]['now'] #дата решения
#         CURRENT_TIME = select_query_dict(db,'select now()', NONE)[0]['now'] #дата решения
#         cursor = db.cursor()
#         cursor.execute(
#             'INSERT INTO assignment (id_student, solving_data, time_start, id_exercise, usage_mode)'
#             ' VALUES (%s, %s, %s, %s, %s)',
#             (g.user['id'], CURRENT_DATE,CURRENT_TIME, id_exercise, 0)
#         )
#         id_assignment = select_query_dict(db,'select count(id) from assignment',NONE)[0]['count']
#         cursor = db.cursor()
#         cursor.execute(
#             'INSERT INTO solution_attempts (id_assignment, sql_query, posting_time, condition, result, message_error, execution_time)'
#             ' VALUES (%s, %s, %s, %s, %s, %s, %s)',
#             (id_assignment, body ,CURRENT_TIME,0,0,"", 13)
#         )
#         db.commit()
        
#         cursor.close()

#         flash('Answer written')

#         # # db = get_db()
#         # # CURRENT_DATE = select_query_dict(db,'select now()', NONE)[0]['now'] #дата решения
#         # # CURRENT_TIME = select_query_dict(db,'select now()', NONE)[0]['now'] #дата решения
#         # # cursor = db.cursor()
#         # # cursor.execute(
#         # #     'INSERT INTO assignment (id_student, solving_data, time_start, id_exercise, usage_mode)'
#         # #     ' VALUES (%s, %s, %s, %s, %s)'
#         # #     ' RETURNING id',
#         # #     (g.user['id'], CURRENT_DATE,CURRENT_TIME, id_exercise, 0)
#         # # )
#         # # id_assignment = cursor.fetchone()[0]
#         # # #id_assignment = select_query_dict(db,'select count(id) from assignment',NONE)[0]['count']
#         # # write_solution_attempt(id_assignment)
#         # # # cursor = db.cursor()
#         # # # cursor.execute(
#         # # #     'INSERT INTO solution_attempts (id_assignment, sql_query, posting_time, condition, result, message_error, execution_time)'
#         # # #     ' VALUES (%s, %s, %s, %s, %s, %s, %s)',
#         # # #     (id_assignment, body ,CURRENT_TIME,0,0,"", 13)
#         # # # )
#         # # db.commit()
        
#         # # cursor.close()

#         # # flash('Answer written')
#     data = select_query_dict(db,'select exercise.* from exercise where exercise.id in (select ability_defence.id_exercise from ability_defence  where ability_defence.id_student = (select distinct student.id from student join users on student.id_user = users.id  where username = %s))', [g.user['username']])
#     return render_template('work_mode/student_have_exercises.html', data = data)

@bp.route('/wmode/test/start',methods=('GET',))
def test_start_get():

    db = get_db() #обращение к базе данных

    exercise = select_query_dict(db,                                                                 
        'select exercise.* '
        'from assignment '
        'left join solution_attempts on assignment.id = solution_attempts.id_assignment '
        'join student on student.id = assignment.id_student '
        'join users on users.id = student.id_user '
        'join exercise on exercise.id = assignment.id_exercise '
        'join category on category.id = exercise.category_id '
        'where solution_attempts.id_assignment is not null and assignment.usage_mode = 2 and users.id = %s ', [g.user['id']])#выборка задач которые решал студент(контрольных)
    if len(exercise) != 0:#если решал
        flash("Вы уже отправили решение.") 
        return render_template('work_mode/test.html')
        
    id_assignment = select_query_dict(db,                                                                 
        'select assignment.id '
        'from assignment '
        'join student on student.id = assignment.id_student '
        'join users on users.id = student.id_user '
        'left join solution_attempts on assignment.id = solution_attempts.id_assignment '
        'where solution_attempts.id_assignment is null and assignment.usage_mode = 2 and users.id = %s ', [g.user['id']])#выборка идентификатор недорешенного задания
    
    
    if(len(id_assignment)):
        return render_template('work_mode/have_unsolved_problems_test.html')
    group = select_query_dict(db, 'select grp from student '
                                      'join users on users.id = student.id_user '
                                      'where users.id = %s ', [g.user['id']])[0]['grp']#группа студента
    CURRENT_DATE = select_query_dict(db,'select now()', NONE)[0]['now'] #дата решения
    CURRENT_TIME = select_query_dict(db,'select now()', NONE)[0]['now'] #дата решения
    cursor = db.cursor()
    cursor.execute(
        'INSERT INTO assignment (id_student, solving_data, time_start, id_exercise, usage_mode)'
        ' VALUES (%s, %s, %s, %s, %s) '
        'returning id',
        (g.user['id'], CURRENT_DATE,CURRENT_TIME, int(choice(select_query_dict(db, 'select distinct(exercise.id) '
                                                                                   'from exercise '
                                                                                   'join category on category.id = exercise.category_id '
                                                                                   'where category.id = 99 '
                                                                                   'except  '
                                                                                   'select assignment.id_exercise  '
                                                                                   'from assignment '
                                                                                   'join student on student.id = assignment.id_student '
                                                                                   'join users on users.id = student.id_user '
                                                                                   'where usage_mode = 2 and student.grp = %s ', [group]))['id']), 2))#выдача задания, запись в бд
    id_assignment = cursor.fetchone()[0]
    db.commit()
    cursor.close()
    exercise = select_query_dict(db,                                                                 
        'select exercise.* '
        'from exercise join assignment on exercise.id = assignment.id_exercise where assignment.id = %s ', [id_assignment])
#блок проверки времени(с момента начала решения задачи студенту дается 90 минут)
    time = select_query_dict(db,                                                                 
        'select assignment.time_start '
        'from assignment '
        'join student on student.id = assignment.id_student '
        'join users on users.id = student.id_user '
        'where users.id = %s and assignment.usage_mode = 2', [g.user['id']])

    t1 = (time[0]['time_start'])
    t2 = (datetime.datetime.now())
    timelimit = (t2-t1).total_seconds() * 1000

    if timelimit > 90* 1000 * 60:
        flash("Время на решение задния вышло")
        return render_template('work_mode/test.html')

    return render_template('work_mode/test_start.html', exercise = exercise[0], timelimit = timelimit)

@bp.route('/wmode/test/continue',methods=('GET',))
def test_continue_get(): 
    db = get_db()#обращение к бд
   
    exercise = select_query_dict(db,                                                                 
        'select exercise.* '
        'from assignment '
        'left join solution_attempts on assignment.id = solution_attempts.id_assignment '
        'join student on student.id = assignment.id_student '
        'join users on users.id = student.id_user '
        'join exercise on exercise.id = assignment.id_exercise '
        'join category on category.id = exercise.category_id '
        'where solution_attempts.id_assignment is not null and assignment.usage_mode = 2 and users.id = %s ', [g.user['id']]) #выборка решенного задания
    if len(exercise) != 0:#если отправлял решение, сообщение студенту
        flash("Вы уже отправили решение.")
        return render_template('work_mode/test.html')
    exercise = select_query_dict(db,                                                                 
        'select exercise.* '
        'from assignment '
        'left join solution_attempts on assignment.id = solution_attempts.id_assignment '
        'join student on student.id = assignment.id_student '
        'join users on users.id = student.id_user '
        'join exercise on exercise.id = assignment.id_exercise '
        'join category on category.id = exercise.category_id '
        'where solution_attempts.id_assignment is null and assignment.usage_mode = 2 and users.id = %s ', [g.user['id']])#выборка недорешенного задания
    if len(exercise) == 0:#если не получил задание, сообщение студенту
        flash("Вы еще не приступали к заданиям.")
        return render_template('work_mode/test.html')

#блок проверки времени(с момента начала решения задачи студенту дается 90 минут)
    time = select_query_dict(db,                                                                 
        'select assignment.time_start '
        'from assignment '
        'join student on student.id = assignment.id_student '
        'join users on users.id = student.id_user '
        'where users.id = %s and assignment.usage_mode = 2', [g.user['id']])
    t1 = (time[0]['time_start'])
    t2 = (datetime.datetime.now())
    timelimit = (t2-t1).total_seconds() * 1000
    if timelimit > 90* 1000 * 60:
        flash("Время на решение задния вышло")
        return render_template('work_mode/test.html')
    
    return render_template('work_mode/test_continue.html', exercise = exercise[0], timelimit = int(timelimit/1000/60))

@bp.route('/wmode/test/start',methods=('POST',))
@bp.route('/wmode/test/continue',methods=('POST',))
def test_start_and_continue_post():
    db = get_db()#обращение к бд

    body = request.form['body']#текст ответа студента

    error = None
    id_assignment = select_query_dict(db,                                                                 
        'select assignment.id '
        'from assignment '
        'join student on student.id = assignment.id_student '
        'join users on users.id = student.id_user '
        'left join solution_attempts on assignment.id = solution_attempts.id_assignment '
        'where  solution_attempts.id_assignment is null and assignment.usage_mode = 2 and users.id = %s ', [g.user['id']])#выбор номера задания который решал студент

    if not body:#если не записан ответ
        error = 'Вы не записали ответ'
        number_of_ex_to_continue = select_query_dict(db,                                                                 
        'select exercise.* '
        'from assignment '
        'join exercise on exercise.id = assignment.id_exercise '
        'where assignment.id = %s ',[int(id_assignment[0]['id'])])
    if error is not None:
        flash(error)#сообщение студенту о пустом ответе
        return render_template('work_mode/test_continue.html', exercise = number_of_ex_to_continue[0])
    else:#если ответ записан
            
        write_solution_attempt(int(id_assignment[0]['id']), body, 0)#запись ответа студентач
        return render_template('work_mode/test.html')

    return render_template('work_mode/test.html')

@bp.route('/wmode/defence',methods=('get',))
def defence_get():
    return render_template('work_mode/defence.html')

@bp.route('/wmode/test',methods=('get',))
def test_get():
    return render_template('work_mode/test.html')

@bp.route('/wmode/debug',methods=('get',))
def debug_get():
    return render_template('work_mode/debug.html')  

@bp.route('/wmode/debug/start',methods=('GET',))
def debug_start_get():
    db = get_db()

    id_assignment = select_query_dict(db,                                                                 
        ' select assignment.id '
        ' from assignment  '
        ' left join solution_attempts on assignment.id = solution_attempts.id_assignment  '
        ' join student on student.id = assignment.id_student  '
        ' join users on users.id = student.id_user  '
        ' join exercise on exercise.id = assignment.id_exercise ' 
        ' join category on category.id = exercise.category_id  '
        ' where assignment.usage_mode = 1 and users.id = %s '
        'except '
        'select assignment.id '
        ' from assignment  '
        ' left join solution_attempts on assignment.id = solution_attempts.id_assignment  '
        ' join student on student.id = assignment.id_student  '
        ' join users on users.id = student.id_user  '
        ' join exercise on exercise.id = assignment.id_exercise  '
        ' join category on category.id = exercise.category_id  '
        ' where  solution_attempts.result = 0 and assignment.usage_mode = 1 and users.id = %s ', [g.user['id'], g.user['id']]) #выборка недорешанных задач
    
    if(len(id_assignment)):#если есть задача, студенту сообщается об этом, иначе отрисовывается страница с выбором задания для отладки
        return render_template('work_mode/have_unsolved_problems_debug.html')
        
    exercise = [{}]
    return render_template('work_mode/debug_start.html', exercise = exercise)

@bp.route('/wmode/debug/continue',methods=('GET',))
def debug_continue_get():
    db = get_db()#доступ к бд
    exercise_id = select_query_dict(db,                                                                 
        ' select assignment.id_exercise '
        ' from assignment  '
        ' left join solution_attempts on assignment.id = solution_attempts.id_assignment  '
        ' join student on student.id = assignment.id_student  '
        ' join users on users.id = student.id_user  '
        ' join exercise on exercise.id = assignment.id_exercise ' 
        ' join category on category.id = exercise.category_id  '
        ' where assignment.usage_mode = 1 and users.id = %s '
        'except '
        'select assignment.id_exercise '
        ' from assignment  '
        ' left join solution_attempts on assignment.id = solution_attempts.id_assignment  '
        ' join student on student.id = assignment.id_student  '
        ' join users on users.id = student.id_user  '
        ' join exercise on exercise.id = assignment.id_exercise  '
        ' join category on category.id = exercise.category_id  '
        ' where  solution_attempts.result = 0 and assignment.usage_mode = 1 and users.id = %s ', [g.user['id'], g.user['id']])#выборка недорешенного задания 
    if len(exercise_id) == 0:#если недорешенного задания нет, об этом сообщается студенту
         flash("Вы еще не приступали к заданиям.")
         return render_template('work_mode/debug.html')
    #если недорешенное задание есть, студент получает его и продолжает работу
    exercise =  select_query_dict(db,  'select * from exercise where id = %s', [exercise_id[0]['id_exercise']])

    return render_template('work_mode/debug_continue.html', exercise = exercise)

@bp.route('/wmode/debug/continue',methods=('POST',))
def debug_continue_post():
    db = get_db()#обращеник бд
    
    body = request.form['body']#текст введенный студентом

    error = None

    id_assignment = select_query_dict(db,                                                                 
        ' select assignment.id '
        ' from assignment  '
        ' left join solution_attempts on assignment.id = solution_attempts.id_assignment  '
        ' join student on student.id = assignment.id_student  '
        ' join users on users.id = student.id_user  '
        ' join exercise on exercise.id = assignment.id_exercise ' 
        ' join category on category.id = exercise.category_id  '
        ' where assignment.usage_mode = 1 and users.id = %s '
        'except '
        'select assignment.id '
        ' from assignment  '
        ' left join solution_attempts on assignment.id = solution_attempts.id_assignment  '
        ' join student on student.id = assignment.id_student  '
        ' join users on users.id = student.id_user  '
        ' join exercise on exercise.id = assignment.id_exercise  '
        ' join category on category.id = exercise.category_id  '
        ' where  solution_attempts.result = 0 and assignment.usage_mode = 1 and users.id = %s ', [g.user['id'], g.user['id']])#выборка номера решаемого задания
    if not body:#проверка ответа на пустотку
        error = 'Вы не ввели ответ'
        number_of_ex_to_continue = select_query_dict(db,                                                                 
        'select exercise.* '
        'from assignment '
        'join exercise on exercise.id = assignment.id_exercise '
        'where assignment.id = %s ',[int(id_assignment[0]['id'])])
    if error is not None:#сообщение о пустом ответе
        flash(error)

        return render_template('work_mode/debug_continue.html', exercise = number_of_ex_to_continue)
    else:
        #если ответ не путой, то делается записьв бд, ответ студента ставится в очередь на проверку
        write_solution_attempt(int(id_assignment[0]['id']), body, 0)
        return render_template('work_mode/debug.html')

    return render_template('work_mode/debug.html')

@bp.route('/wmode/debug/start',methods=('POST',))
def debug_start_post():
    db = get_db()
    exercise = [{}]

    number_lr = request.form['number_lr']#номер лабораторной работы
    variant = request.form['variant']#вариант
    assignment_number_lr = request.form['assignment_number_lr']#номер задания в лабораторной работе

    body = request.form['body']# текст запроса студента
    #всячекие проверки на корректность ввода 
    if number_lr == '' or variant == '' or assignment_number_lr == '':
        flash("вы не полностью ввели данные для выборки задания")
        return render_template('work_mode/debug_start.html', exercise = exercise)   
    try:
        int(number_lr)
        int(variant)
        int(assignment_number_lr)
    except ValueError:
        flash("вы неверно ввели данные для выборки задания")
        return render_template('work_mode/debug_start.html', exercise = exercise)

    exercise = select_query_dict(db,'select exercise.* from exercise   '
                            'join category on category.id = exercise.category_id  '
                            'where exercise.variant_number_lr = %s and category.number_lr = %s and category.assignment_number_lr = %s ', [variant, number_lr, assignment_number_lr ])
                            #выборка упражнения для отладки повведенным студентом данным
    #если такого упражнения нет, то страница не обновлятеся 
    if len(exercise) == 0:
        exercise = [{}]
        return render_template('work_mode/debug_start.html', exercise = exercise)
    
    if len(request.form) == 5:#если нажали на кнопку взять задание
        
        id_assignment = select_query_dict(db,    #проверка взяли ли уже задание                                                             
        'select assignment.id '
        'from assignment '
        'join student on student.id = assignment.id_student '
        'join users on users.id = student.id_user '
        'left join solution_attempts on assignment.id = solution_attempts.id_assignment '
        'where  solution_attempts.id_assignment is null and assignment.usage_mode = 1 and users.id = %s ', [g.user['id']])


        if (len(id_assignment)): #перезапись задания
            exid = exercise[0]['id']
            cursor = db.cursor()
            cursor.execute('update assignment set id_exercise = %s where id = %s ', (exid, id_assignment[0]['id']))
            db.commit()
            cursor.close()
            return render_template('work_mode/debug_start.html', exercise = exercise)

        if request.form['take_for_debbag'] == 'Взять задание на отладку': #запись в базу данных задания полученное студентом 
                CURRENT_DATE = select_query_dict(db,'select now()', NONE)[0]['now'] #дата решения
                CURRENT_TIME = select_query_dict(db,'select now()', NONE)[0]['now'] #дата решения
                cursor = db.cursor()
                cursor.execute(
                    'INSERT INTO assignment (id_student, solving_data, time_start, id_exercise, usage_mode)'
                    ' VALUES (%s, %s, %s, %s, %s) '
                    'returning id',
                    (g.user['id'], CURRENT_DATE,CURRENT_TIME, exercise[0]['id'], 1)
                )
                id_assignment = cursor.fetchone()[0]
                db.commit()
                cursor.close()
    if len(request.form['body']) == 0:
        return render_template('work_mode/debug_start.html', exercise = exercise)
    else:
        id_assignment = select_query_dict(db,                                                                 
        'select assignment.id '
        'from assignment '
        'join student on student.id = assignment.id_student '
        'join users on users.id = student.id_user '
        'left join solution_attempts on assignment.id = solution_attempts.id_assignment '
        'where  solution_attempts.id_assignment is null and assignment.usage_mode = 1 and users.id = %s ', [g.user['id']])[0]
        
        write_solution_attempt(int(id_assignment['id']), request.form['body'], 0)#запись попытки решения студента в бд
        
        return render_template('work_mode/debug.html')


@bp.route('/wmode/mysolutions',methods=('GET',))
def mysolutions_get():
    db = get_db()

    solutions = select_query_dict(db,                                                                 
        'select solution_attempts.sql_query, solution_attempts.condition ,solution_attempts.result, solution_attempts.message_error, assignment.usage_mode, exercise.problem_statement   '             
        'from assignment '
        'right join solution_attempts on assignment.id = solution_attempts.id_assignment '
        'left join exercise on exercise.id = assignment.id_exercise '
        'where assignment.id_student = %s and assignment.usage_mode <> 2', [g.user['id']])
    for sol in solutions:
        if sol['usage_mode'] == 0:
            sol['usage_mode'] = 'Решение задач по теме'
        if sol['usage_mode'] == 1:
            sol['usage_mode'] = 'Отладка запроса'
        if sol['usage_mode'] == 3:
            sol['usage_mode'] = 'Защита лаботорной работы'
        if sol['message_error'] == None:
            sol['message_error'] = ""
        if (sol['condition'] == 0) or (sol['condition'] == 1):
            sol['condition'] = "На проверке"
        if sol['condition'] == 2:
            sol['condition'] = "Проверено"
        if sol['message_error'] == None:
            sol['message_error'] = ""
        if sol['result'] == None:
            sol['result'] = ""
        
        
    return render_template('work_mode/mysolutions.html', solutions = solutions, id_praktiki = int(request.args['id_praktiki']), token = request.args['token'])

def write_solution_attempt(id_assignment:int,
sql_query:str,
condition:int=0,
#posting_time=None,
result:int=999,
# message_error:str=None,
# execution_time=None,
):
    """
    Добавляет попытку студента в бд и ставит в очередь на проверку

    Parameters:
        id_assignment (int): id задания для студента из таблицы assignment
        sql_queery (str): ответ студента
        condition (int): состояние
    Returns:
        `0` - on success
        `not 0` - on fail
    """

    if type(sql_query) is not str:
        print("sql_qury is not a string")
        return 1
    if type(id_assignment) is not int:
        print("id_assignment is not an int")
        return 2
    if type(condition) is not int:
        print("condition is not an int")
        return 3
    db = get_db()
    cursor = db.cursor()
    cursor.execute(
            'INSERT INTO solution_attempts (id_assignment, sql_query, condition, result)'
            ' VALUES (%s, %s, %s, %s)'
            'RETURNING id',
            (id_assignment, sql_query ,condition, result)
        )
    solution_id = cursor.fetchone()[0]
    db.commit()
    cursor.close()
    #add task to queue
    msg_queue = get_msg_queue()
    channel = msg_queue.channel()
    channel.queue_declare(queue='SQLChecker')
    body = {'usr_id':g.user['id'],'solution_id':solution_id }
    channel.basic_publish(exchange='',
                    routing_key='SQLChecker',
                    body=json.dumps(body))
    
    # flash('Answer written')
    return 0
