from pickle import NONE
import pika
from flask import current_app, g

# connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
# channel = connection.channel()
# channel.queue_declare(queue='SQLChecker')
# channel.basic_publish(exchange='',
#                       routing_key='SQLChecker',
#                       body='Hello World!')
# print(" [x] Sent 'Hello World!'")
# connection.close()


def get_msg_queue():
    if 'msg_queue' not in g:
        connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
        g.msg_queue = connection
    return g.msg_queue

def close_msg_queue():
    msg_queue = g.pop('msg_queue', NONE)

    if msg_queue is not None:
        msg_queue.close()

# def init_msg_queue():
    