from calendar import c
import functools
from sqlchecker.testModeAPI import test_start_get
from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash, generate_password_hash

from sqlchecker.db import get_db, select_query_dict

bp = Blueprint('api', __name__, url_prefix='/api')

@bp.route('/wmode/test/start',methods=('GET',))
def test_start_get_api():
    """

    code == 0 - задача получена успешно
    code == 1 - решение уже записано.
    code == 2 - backend error

    """

    flag = 0
    exercise = test_start_get(flag)
    response = {
        'code' : 0,
        'exercise': exercise}
    if flag == 0:
        if len(exercise) != 0:
            # flash("вы уже отправили решение")
            response['code'] = 1
            return response
        else:
            return response
    if flag == 1:
        response['code'] = 2
        return response