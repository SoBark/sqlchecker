#!/usr/bin/env python
import json
from pickle import NONE
from xml.etree.ElementTree import tostring
import pika, sys, os
import re
import sqlparse
import answer_сheck

def main():
    #подключение к очереди сообщений
    msg_connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    msg_channel = msg_connection.channel()

    msg_channel.queue_declare(queue='SQLChecker')
    
    #определение колбэк функции для обработки получения
    #сообщения из очереди сообщения 
    def callback(ch, method, properties, body):
        #получения сообщения
        ids = json.loads(body)
        error = NONE
        #Проверяем, содержим ли сообщение нужные данные
        #id пользователя
        if not 'usr_id' in ids:
            error = 'User id not send.'
        #id решения
        if not 'solution_id' in ids:
            error = 'solution_id not send'
        #Если сообщения не содержит нужных данных, вывести сообщение об ошибке
        if error is not NONE:
            print(error)
        #Если необходимые данные для начала проверки получены
        else:
            print(" [x] Received %r" % ids)

            #Начинаем проверку ответа данного студентом
            #Если в процессе проверки обнаружится ошибка
            #Метод поднимет исключение
            try:
                answer_сheck.check_solution(solution_id=ids['solution_id'])
            except BaseException as err:
                print('Exception', err)
    #Задаем параметры для общения с очередью сообщений
    msg_channel.basic_consume(queue='SQLChecker', on_message_callback=callback, auto_ack=True)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    #Начинаем слушать очередь
    msg_channel.start_consuming()

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)