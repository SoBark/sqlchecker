import psycopg2
import os
import configparser
#irom flask import current_app, g

config = configparser.ConfigParser()
with open('instance/config.py', 'r') as configfile:
  config.read_string("[top]\n" + configfile.read())
def get_db_answers():
    db = psycopg2.connect(
        host='localhost',
        database="flask_db",
        # user=os.environ['DB_ANSWERS_USERNAME'],
        # password=os.environ['DB_ANSWERS_PASSWORD'],
        user=config['top']['DB_ANSWERS_USERNAME'].strip('"'),
        password=config['top']['DB_ANSWERS_PASSWORD'].strip('"'),
    )
    return db

def close_db(db):
    db.close()


def get_db_presets_student():
    db = psycopg2.connect(
        host='localhost',
        database="presets_db",
        user=config['top']['DB_PRESETS_USERNAME_STUDENT'].strip('"'),
        password=config['top']['DB_PRESETS_PASSWORD_STUDENT'].strip('"'),
        # user=os.environ['DB_PRESETS_USERNAME_STUDENT'],
        # password=os.environ['DB_PRESETS_PASSWORD_STUDENT'],
    )
    return db

def get_db_presets_admin():
    db = psycopg2.connect(
        host='localhost',
        database="presets_db",
        user=config['top']['DB_PRESETS_USERNAME_ADMIN'].strip('"'),
        password=config['top']['DB_PRESETS_PASSWORD_ADMIN'].strip('"'),
        # user=os.environ['DB_PRESETS_USERNAME_ADMIN'],
        # password=os.environ['DB_PRESETS_PASSWORD_ADMIN'],
    )
    return db

def init_db_presets():
    db = get_db_presets_admin()
    cur = db.cursor()
    work_dir = os.getcwd()
    dir = work_dir.split('/')[-1]
    if dir != 'answerChecker':
        os.chdir(work_dir + "/answerChecker")

    with open('./schema_presets.sql', encoding = 'utf-8') as f:
        cur.execute(f.read())
    db.commit()
    close_db(db)
    print ("Presets database initialized.")
    os.chdir(work_dir)



def select_query_dict(connection, query, data=[]):
    """
    Run generic select query on db, returns a list of dictionaries
    """
    #logger.debug('Running query: {}'.format(query))

    # Open a cursor to perform database operations
    cursor = connection.cursor()
    #logging.debug('Db connection succesful')

    # execute the query
    try:
        #logger.info('Running query.')
        if len(data):
            cursor.execute(query, data)
        else:
            cursor.execute(query)
        columns = list(cursor.description)
        result = cursor.fetchall()
        #logging.debug('Query executed succesfully')
    except (Exception, psycopg2.DatabaseError) as e:
        #logging.error(e)
        print(e)
        cursor.close()
        exit(1)

    cursor.close()

    # make dict
    results = []
    for row in result:
        row_dict = {}
        for i, col in enumerate(columns):
            row_dict[col.name] = row[i]
        results.append(row_dict)

    return results