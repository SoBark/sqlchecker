#import answerChecker.database as db
#import database as db
import pyparsing
import re
import itertools
import psycopg2
import sqlparse
import os, sys
from database import get_db_answers, get_db_presets_admin, get_db_presets_student
from typing import List, Tuple
from psycopg2 import errors as bd_errors
import datacompy
import pandas as pd
from time import time
import numpy as np

STATUS_OF_CHECKING = {
    'sended': 0,
    'checking': 1,
    'checked': 2,
    'interrupted': 3,
    #'failed_with_error': 4,
}

USAGE_MODE = {
    'trainer': 0,
    'lab_defence': 1,
    'control_work': 2,
    'lab_debug': 3,
}

RESULT_OF_CHECKING = {
    'correct': 0,
    'syntax_error': 100,
    'query_runtime_error': 101,
    'query_incorrect_default_table' : 102,
    'query_incorrect_alt_table': 103,
    'query_contains_several_queries': 104,


    'other_exception': 666,
}



class CheckingError(Exception):
    """Class for errors ocuring during solution processing
    
    Parameters
    ----------
        condition : int
            status of checking solution
        result : int
            status of solution result
    """
    condition = STATUS_OF_CHECKING['interrupted']
    result = RESULT_OF_CHECKING['other_exception']

def parse_sql(sql: str):
    if not isinstance(sql,str):
        raise Exception('sql query not a str')
    tmp = sqlparse.split(sql)
    tmp = [x for x in tmp if x!= ';']
    if len(tmp) > 1:
        raise Exception("Correct answer use several queries")
    tmp = tmp[0]
    if tmp[-1:] == ';':
        tmp = tmp[:-1]
    if tmp[:len('select')].lower() != 'select':
        raise Exception('sql query doesn\'t start with "select"')
    tmp = tmp[len('select'):]
    if tmp[0] != ' ' and tmp[0] != '\n':
        raise Exception('sql query has misspelling near "select"') 
    tmp = tmp.lstrip()
    tmp_start = 'select '
    i = 0
    if tmp[:len('distinct')].lower() == 'distinct':
        if tmp[len('distinct')] == ' ' or tmp[len('distinct')] == '\n':
            tmp = tmp[len('distinct'):]
            tmp_start += ' distinct '
    

    while (i < len(tmp)):
        if tmp[i] == '"':
            i = parse_escape_quote(tmp, i, '"')
        if tmp[i] == "'":
            i = parse_escape_quote(tmp, i, "'")
        if tmp[i].lower() == 'f':
            if tmp[i:i+len('from')].lower() == 'from':
                if tmp[i+len('from')] == ' ' or tmp[i+len('from')] == '\n':
                    break
        i+=1
    tmp_end = tmp[i:]
    tmp = tmp[:i].strip()
    COMMA_MATCHER = re.compile(r",(?=(?:[^\"']*[\"'][^\"']*[\"'])*[^\"']*$)")
    tmp = COMMA_MATCHER.split(tmp)
    return  tmp_start, tmp, tmp_end


def parse_escape_quote(tmp:str, i:int, quote:str) -> int:
    i +=1
    while i < len(tmp):
        if (tmp[i] == quote):
            if tmp[i+1] != quote:
                return i
            i+=1
        i+=1
    return i


def compare_answers_except(
    student_query: str,
    query_template_start: str,
    i_q: List[tuple], 
    query_template_end: str,
    db_connection,
    schema: List[tuple]):

    cursor = db_connection.cursor()
    try:
        cursor.execute(f'SET search_path TO {schema};')
    except Exception as err:
        print(err)
        raise err
    
    for combination in list(i_q):
        cor_answ = query_template_start + ' ' + ','.join(combination) + ' ' + query_template_end
        try:
            q_1 = f"""({cor_answ})
            EXCEPT
            ({student_query})"""
            cursor.execute(q_1)
            answer_1 = cursor.fetchall()
            cursor.execute(f"""({student_query}) 
            EXCEPT
            ({cor_answ})""")
            answer_2 = cursor.fetchall()
            if len(answer_1) != 0 or len(answer_2) != 0:
                i_q.remove(combination)        
        except bd_errors.DatatypeMismatch as err:
            #print(type(err), err)
            i_q.remove(combination)
            db_connection.rollback()
            #cursor.execute(f'SET search_path TO {schema};')
        except (Exception, psycopg2.DatabaseError) as err:
            print(type(err),err)
            cursor.close()
            raise (err)

    cursor.close()

def check_solution(solution_id: int):
    """Check student solution
    
    It takes student solution from database and compare
    it result with correct result, which contains as json inside the database.
    Results of comparisons writeback to database by updating solution status and other parameters

    Parameters
    ----------
        solution_id: int
          id of student's solution 
    """
    #Получаем запись из таблицы Попытка решения
    try:
        solution = get_solution(solution_id)
    except ValueError as err:
        print(err)
        return
    #Устанавливаем поле состояние из таблицы Попытка решения
    #В "идет проверка"  
    set_condition(solution_id, condition=STATUS_OF_CHECKING['checking'])
    try:
        #Если запрос сосит из нескольких отдельных запросов
        #Получаем их список
        statetments = sqlparse.split(solution["sql_query"])
        #Убираем "пустые" запросы
        statetments = [x for x in statetments if x!= ';']
        #Если запрос состоит из нескольких отдельных запросов
        #Поднимаем исключение
        if len(statetments) > 1:
            err = CheckingError(f"Solution with id {solution_id} containts several_queries")
            err.condition = STATUS_OF_CHECKING['interrupted']
            err.result = RESULT_OF_CHECKING['query_contains_several_queries']
            raise err
        #SQL-запрос студента
        student_query = statetments[0]
        #Получаем задание и набор альтернативных таблицы для задания
        exercise, alt_tables = get_right_answers(solution['id_assignment'])
        #Начинаем проверку запроса
        execution_time = compare_answers(student_query, exercise, alt_tables)

    except CheckingError as err:
        #Корректируем запись в таблице Попытка решения в случае известной ошибки при проверке
        set_errormsg(solution_id=solution_id, condition=err.condition, result=err.result, message_error=str(err))
    except Exception as err:
        #Корректируем запись в таблице Попытка решения в случае непредвиденной ошибки при проверке
        set_errormsg(solution_id=solution_id, condition=STATUS_OF_CHECKING['interrupted'], result=RESULT_OF_CHECKING['other_exception'], message_error=str(err))
        raise err
    else:
        #Корректируем запись в таблице Попытка решения в случае успешного прохождения проверки
        set_execution_time(solution_id=solution_id, condition=STATUS_OF_CHECKING['checked'], result=RESULT_OF_CHECKING['correct'], execution_time=execution_time)
    finally:
        #print(f"Checking solution with id {solution_id} ended")
        pass

def set_execution_time(solution_id:int, condition:int, result:int,  execution_time:float):
    """Update solution, with new condition, result and execution_time
    
    Parameters
    ----------
    solution_id: int
        id of student's solution 
    condition: int
        status of checking.
    execution_time: float
        maximum execution_time on one of tables
    """
    db_answers = get_db_answers()
    cursor = db_answers.cursor()
    cursor.execute(
        """ UPDATE solution_attempts 
            SET condition = %s,
                result = %s,
                execution_time = %s
            WHERE id = %s
        """, (condition, result, execution_time, solution_id))
    db_answers.commit()
    cursor.close()

def set_errormsg(solution_id:int, condition:int, result:int, message_error:str):
    """Update solution, with new condition, result and message_error
    
    Parameters
    ----------
    solution_id: int
        id of student's solution 
    condition: int
        status of checking.
    message_error: str
        error message
    """
    db_answers = get_db_answers()
    cursor = db_answers.cursor()
    cursor.execute(
        """ UPDATE solution_attempts 
            SET condition = %s,
                result = %s,
                message_error = %s
            WHERE id = %s
        """, (condition, result, message_error, solution_id))
    db_answers.commit()
    cursor.close()


def compare_answers(student_query:str, exercise: dict, alt_tables:List[dict]):
    """Compare student query results with correct results

    Parameters
    ----------
    student_query: str
        student's SQL request
    exercise: dict
        assigned exercise
    alt_tables: List[dict]
        list of alternative tables for exercise
    
    """
    #TODO check security concerns
    #TODO request to different databases structers
    db_presets_student = get_db_presets_student()
    cursor_student = db_presets_student.cursor()
    execution_time = 0.0
    try:
        #Выполняем запрос на базовом наборе таблиц
        tic = time()
        cursor_student.execute(student_query)
        student_result = cursor_student.fetchall()
        toc = time()
        execution_time = max(toc-tic, execution_time)
    except psycopg2.errors.SyntaxError as e:
        #Если запрос синтаксически неверен поднимаем исключение
        db_presets_student.rollback()
        cursor_student.close()
        err = CheckingError(str(e))
        err.condition = STATUS_OF_CHECKING['interrupted']
        err.result = RESULT_OF_CHECKING['syntax_error']
        raise err
    try:
        columns = list(cursor_student.description)
        db_presets_student.rollback()
        for j, row in enumerate(student_result):
            student_result[j] = { i:row[i] for i in range(len(columns))}
        student_result = pd.DataFrame(student_result)
        correct_result = pd.DataFrame(exercise['right_result'])
        #Сравниваем результаты запросов на базовом наборе
        labels = compare_query_results(student_result=student_result, correct_result=correct_result, result=RESULT_OF_CHECKING['query_incorrect_default_table'], is_sort=exercise['is_sort'])
        for x in alt_tables:
            #Сравнивам результаты на альтернативных наборах данных
            cursor_student.execute(x['textsql'])
            tic = time()
            cursor_student.execute(student_query)
            student_result = cursor_student.fetchall()
            toc = time()
            execution_time = max(toc-tic, execution_time)
            db_presets_student.rollback()
            for j, row in enumerate(student_result):
                student_result[j] = { i:row[i] for i in range(len(columns))}
            student_result = pd.DataFrame(student_result)
            correct_result = pd.DataFrame(x['right_result'])
            #Сравниваем результаты запросов на альтернативной таблице
            compare_query_results(student_result=student_result, correct_result=correct_result, result=RESULT_OF_CHECKING['query_incorrect_alt_table'], is_sort=exercise['is_sort'], labels=labels)
    finally:
        #Убеждаемся что изменения произведенные в транзакции не применились 
        db_presets_student.rollback()
        cursor_student.close()
    return execution_time

def compare_query_results(student_result:pd.DataFrame, correct_result:pd.DataFrame, result: int, is_sort:int, labels: List=None ):
    """Compare sql query results in pandas dataframes"""
    err = CheckingError("Failed on default table" if result == RESULT_OF_CHECKING['query_incorrect_default_table'] else "Failed on alternative table")
    err.condition = STATUS_OF_CHECKING['checked']
    err.result = result
    if student_result.shape != correct_result.shape:
        raise err
    #Если соотвестие между столбцами ещё не подбиралось, то попытаемся его подобрать
    if labels is None:
        n = len(student_result.columns)
        # dict_eq_col = {}
        list_eq_col = []
        columns_student = student_result.columns
        columns_correct = correct_result.columns
        #Сопоставляем столбцы по содержимому
        for i in range(n):
            for j in range(n):
                if (student_result.dtypes[i] != correct_result.dtypes[j]) or (columns_correct[j] in list_eq_col): continue
                if np.array_equal(student_result.iloc[:,i].sort_values().values,correct_result.iloc[:,j].sort_values().values):
                    list_eq_col.append(columns_correct[j])
                    break
            if len(list_eq_col) != i+1: raise err
        if len(list_eq_col) != n: raise err
        labels = list_eq_col
    #Переименновываем столбцы в соотвествие с подобранным соотвествием
    student_result.columns = labels
    if is_sort == 0:
        #Сортируем таблицы, если порядок вывода не важен для задания
        flag = student_result.sort_index(axis=1).sort_values(labels).reset_index(drop=True).equals(correct_result.sort_index(axis=1).sort_values(labels).reset_index(drop=True))
    else:
        #Оставляем таблицы как есть, если порядок вывода важен
        flag = student_result.sort_index(axis=1).equals(correct_result.sort_index(axis=1))
    if not flag: raise err
    #Возвращаем предполагаемое соотвествие столбцов
    return labels
    



def get_right_answers(id_assignment: int)-> Tuple[dict,dict]:
    """
    Selects right answers for assigned task

    Retrievs right answers for assignment from database.
        
    Parameters
    ----------
        id_assignment: int
            id of student's assignment

    Returns
    ----------
        tuple(dict, dict), where first dict is row from exercise and second is rows for exercise from alternative_sets_of_tables

    """
    db_answers = get_db_answers()
    cursor = db_answers.cursor()
    cursor.execute(
        """ 
        SELECT * FROM exercise
        WHERE exercise.id = (
            SELECT id_exercise FROM assignment WHERE id = %s
            );
        """, (id_assignment,))
    exercise = cursor.fetchone()
    if exercise is None:
        cursor.close()
        err = CheckingError(f"There is problem with retriving exercise for assignment with id {id_assignment}")
        err.condition = STATUS_OF_CHECKING['interrupted']
        err.result = RESULT_OF_CHECKING['other_exception']
        raise err
    columns = list(cursor.description)
    exercise = { col.name:exercise[i] for i, col in enumerate(columns)}
    cursor.execute(
        """ 
        SELECT * FROM alternative_sets_of_tables
        WHERE id_exercise = %s;
        """, (exercise['id'],))
    alt_tables = cursor.fetchall()
    columns = list(cursor.description)
    for j, row in enumerate(alt_tables):
        alt_tables[j] = { col.name:row[i] for i, col in enumerate(columns)}

    db_answers.commit()
    cursor.close()
    return exercise, alt_tables


def get_solution(solution_id) -> dict: 
    """ Get student solution from database

        Retrievs student solution from database.
        
        Parameters
        ----------
            solution_id: int
                id of student's solution 

        Returns
        ----------
            dict with solution
    """

    db_answers = get_db_answers()
    cursor = db_answers.cursor()
    cursor.execute(
        """ SELECT * from solution_attempts 
            WHERE id = %s
        """, (solution_id,))
    solution = cursor.fetchone()
    if solution is None:
        cursor.close()
        err = CheckingError(f"There is no solution with id {solution_id}")
        err.condition = STATUS_OF_CHECKING['interrupted']
        err.result = RESULT_OF_CHECKING['other_exception']
        raise err
    columns = list(cursor.description)
    db_answers.commit()
    cursor.close()
    solution = { col.name:solution[i] for i, col in enumerate(columns)}
    return solution


def set_condition(solution_id, condition):
    """
    Set condition for solution in check

        Parameters
    ----------
        solution_id: int
          id of student's solution
        condition: int
            status of checking. `0` - sended, `1` - checking, `2` - checked, `3` - interrupted, `4` - failed with error

    """
    db_answers = get_db_answers()
    cursor = db_answers.cursor()
    cursor.execute(
        """ UPDATE solution_attempts 
            SET condition = %s
            WHERE id = %s
        """, (condition, solution_id))
    db_answers.commit()
    cursor.close()


def main():
    query = '''select    p.town   townp,  j.town  townj, spj.kol,  p.ves
    from  spj
    join  p  on  p.n_det = spj.n_det
    join  j  on  j.n_izd = spj.n_izd
    order  by 1,3      ;'''
    # statetments = sqlparse.split(query)
    # print([x for x in statetments if x!= ';'])
    # print(query[:-1])
    #{"usr_id": 1, "solution_id": 71}
    # solution_id = 214
    solution_id = 4
    check_solution(solution_id)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)


def check_query_depricated(correct_query:str, student_query:str):


    #check student query if it consists of only one statment
    statetments = sqlparse.split(student_query)
    statetments = [x for x in statetments if x!= ';']
    if len(statetments) > 1:
        raise Exception("You couldn't use several queries")
    student_query = statetments[0]
    if student_query[-1:] == ';':
        student_query = student_query[:-1]
    
    #get columns from correct query
    try:
        query_template_start, columns, query_template_end = parse_sql(correct_query)
    except Exception as err:
        print('Parsing ended with exception: ', err)
        raise err
    #get all combinations of columns
    i_q =  [x for x in itertools.permutations(columns)]
    db_connection = db.get_db_presets()
    cursor = db_connection.cursor()
    #get schemas on which we will check
    #TO DO make not factorial algorith proverki
    cursor.execute("SELECT schema_name FROM information_schema.schemata;")
    schemas = cursor.fetchall()
    schemas = [x[0] for x in schemas if x[0] not in ('pg_catalog', 'information_schema')]
    db_connection.rollback()
    cursor.close()
    for schema in schemas:
        compare_answers_except(student_query, query_template_start, i_q, query_template_end, db_connection, schema)
        db_connection.rollback()
        if (len(i_q) < 1):
            raise Exception('Not correct')
    db.close_db(db_connection)

    return True