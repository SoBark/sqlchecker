DROP SCHEMA IF EXISTS preset_1 CASCADE;
DROP SCHEMA IF EXISTS preset_2 CASCADE;
-- DROP TABLE IF EXISTS SPJ;
-- DROP TABLE IF EXISTS S;
-- DROP TABLE IF EXISTS P;
-- DROP TABLE IF EXISTS J;

CREATE SCHEMA preset_1;
CREATE SCHEMA preset_2;


CREATE TABLE preset_1.S (
    n_post character(6) NOT NULL,     
    name character(20),     
    reiting smallint,     
    town character(20) 
);

CREATE TABLE preset_1.P (     
    n_det character(6) NOT NULL,     
    name character(20),     
    cvet character(7),     
    ves smallint,     
    town character(20) 
);

Create TABLE preset_1.J (
    n_izd char(6) not NULL,
    name char(20),
    town char(20)
);

Create TABLE preset_1.SPJ ( 
    n_post char(6),
    n_det char(6),
    n_izd char(6),
    kol smallint
);

INSERT INTO preset_1.S (n_post, name, reiting, town)
VALUES
    ('S1','Смит',20,'Лондон'),
    ('S2','Джонс',10,'Париж'),
    ('S3','Блейк',30,'Париж'),
    ('S4','Кларк',20,'Лондон'),
    ('S5','Адамс',30,'Афины');

INSERT INTO preset_1.P (n_det, name, cvet, ves, town)
VALUES
    ('P1','Гайка','Красный',12,'Лондон'),
    ('P2','Болт','Зеленый',17,'Париж'),
    ('P3','Винт','Голубой',17,'Рим'),
    ('P4','Винт','Красный',14,'Лондон'),
    ('P5','Кулачок','Голубой',12,'Париж'),
    ('P6','Блюм','Красный',19,'Лондон');

INSERT INTO preset_1.J (n_izd, name, town)
VALUES
    ('J1','Жесткий диск','Париж'),
    ('J2','Перфоратор','Рим'),
    ('J3','Считыватель','Афины'),
    ('J4','Принтер','Афины'),
    ('J5','Флоппи-диск','Лондон'),
    ('J6','Терминал','Осло'),
    ('J7','Лента','Лондон');

INSERT INTO preset_1.SPJ (n_post, n_det, n_izd, kol)
VALUES
    ('S1','P1','J1',200),
    ('S1','P1','J4',700),
    ('S2','P3','J1',400),
    ('S2','P3','J2',200),
    ('S2','P3','J3',200),
    ('S2','P3','J4',500),
    ('S2','P3','J5',600),
    ('S2','P3','J6',400),
    ('S2','P3','J7',800),
    ('S2','P5','J2',100),
    ('S3','P3','J1',200),
    ('S3','P4','J2',500),
    ('S4','P6','J3',300),
    ('S4','P6','J7',300),
    ('S5','P2','J2',200),
    ('S5','P2','J4',100),
    ('S5','P5','J5',500),
    ('S5','P5','J7',100),
    ('S5','P6','J2',200),
    ('S5','P1','J4',100),
    ('S5','P3','J4',200),
    ('S5','P4','J4',800),
    ('S5','P5','J4',400),
    ('S5','P6','J4',500);



CREATE TABLE preset_2.S (
    n_post character(6) NOT NULL,     
    name character(20),     
    reiting smallint,     
    town character(20) 
);

CREATE TABLE preset_2.P (     
    n_det character(6) NOT NULL,     
    name character(20),     
    cvet character(7),     
    ves smallint,     
    town character(20) 
);

Create TABLE preset_2.J (
    n_izd char(6) not NULL,
    name char(20),
    town char(20)
);

Create TABLE preset_2.SPJ ( 
    n_post char(6),
    n_det char(6),
    n_izd char(6),
    kol smallint
);

INSERT INTO preset_2.S (n_post, name, reiting, town)
VALUES
    ('S1','Смит',20,'Лондон'),
    ('S2','Джонс',10,'Париж'),
    ('S3','Блейк',30,'Париж'),
    ('S4','Кларк',20,'Лондон'),
    ('S5','Адамс',30,'Афины');

INSERT INTO preset_2.P (n_det, name, cvet, ves, town)
VALUES
    ('P1','Гайка','Красный',12,'Лондон'),
    ('P2','Болт','Зеленый',17,'Париж'),
    ('P3','Винт','Голубой',17,'Рим'),
    ('P4','Винт','Красный',14,'Лондон'),
    ('P5','Кулачок','Голубой',12,'Париж'),
    ('P6','Блюм','Красный',19,'Лондон');

INSERT INTO preset_2.J (n_izd, name, town)
VALUES
    ('J1','Жесткий диск','Париж'),
    ('J2','Перфоратор','Рим'),
    ('J3','Считыватель','Афины'),
    ('J4','Принтер','Афины'),
    ('J5','Флоппи-диск','Лондон'),
    ('J6','Терминал','Осло'),
    ('J7','Лента','Лондон');

INSERT INTO preset_2.SPJ (n_post, n_det, n_izd, kol)
VALUES
    ('S1','P1','J1',200),
    ('S1','P1','J4',700),
    ('S2','P3','J1',400),
    ('S2','P3','J2',200),
    ('S2','P3','J3',200),
    ('S2','P3','J4',500),
    ('S2','P3','J5',600),
    ('S2','P3','J6',400),
    ('S2','P3','J7',800),
    ('S2','P5','J2',100),
    ('S3','P3','J1',200),
    ('S3','P4','J2',500),
    ('S4','P6','J3',300),
    ('S4','P6','J7',300),
    ('S5','P2','J2',200),
    ('S5','P2','J4',100),
    ('S5','P5','J5',500),
    ('S5','P5','J7',100),
    ('S5','P6','J2',200),
    ('S5','P1','J4',100),
    ('S5','P3','J4',200),
    ('S5','P4','J4',800),
    ('S5','P5','J4',400),
    ('S5','P6','J4',500);