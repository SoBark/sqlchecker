import answerChecker.database as db
import pyparsing
import re
import itertools
import psycopg2
import sqlparse
import os, sys
from typing import List
from psycopg2 import errors as bd_errors

def parse_sql(sql: str):
    if not isinstance(sql,str):
        raise Exception('sql query not a str')
    tmp = sqlparse.split(sql)
    tmp = [x for x in tmp if x!= ';']
    if len(tmp) > 1:
        raise Exception("Correct answer use several queries")
    tmp = tmp[0]
    if tmp[-1:] == ';':
        tmp = tmp[:-1]
    if tmp[:len('select')].lower() != 'select':
        raise Exception('sql query doesn\'t start with "select"')
    tmp = tmp[len('select'):]
    if tmp[0] != ' ' and tmp[0] != '\n':
        raise Exception('sql query has misspelling near "select"') 
    tmp = tmp.lstrip()
    tmp_start = 'select '
    i = 0
    if tmp[:len('distinct')].lower() == 'distinct':
        if tmp[len('distinct')] == ' ' or tmp[len('distinct')] == '\n':
            tmp = tmp[len('distinct'):]
            tmp_start += ' distinct '
    

    while (i < len(tmp)):
        if tmp[i] == '"':
            i = parse_escape_quote(tmp, i, '"')
        if tmp[i] == "'":
            i = parse_escape_quote(tmp, i, "'")
        if tmp[i].lower() == 'f':
            if tmp[i:i+len('from')].lower() == 'from':
                if tmp[i+len('from')] == ' ' or tmp[i+len('from')] == '\n':
                    break
        i+=1
    tmp_end = tmp[i:]
    tmp = tmp[:i].strip()
    COMMA_MATCHER = re.compile(r",(?=(?:[^\"']*[\"'][^\"']*[\"'])*[^\"']*$)")
    tmp = COMMA_MATCHER.split(tmp)
    return  tmp_start, tmp, tmp_end


def parse_escape_quote(tmp:str, i:int, quote:str) -> int:
    i +=1
    while i < len(tmp):
        if (tmp[i] == quote):
            if tmp[i+1] != quote:
                return i
            i+=1
        i+=1
    return i


def check_query(correct_query:str, student_query:str):


    #check student query if it consists of only one statment
    statetments = sqlparse.split(student_query)
    statetments = [x for x in statetments if x!= ';']
    if len(statetments) > 1:
        raise Exception("You couldn't use several queries")
    student_query = statetments[0]
    if student_query[-1:] == ';':
        student_query = student_query[:-1]
    
    #get columns from correct query
    try:
        query_template_start, columns, query_template_end = parse_sql(correct_query)
    except Exception as err:
        print('Parsing ended with exception: ', err)
        raise err
    #get all combinations of columns
    i_q =  [x for x in itertools.permutations(columns)]
    db_connection = db.get_db_presets()
    cursor = db_connection.cursor()
    #get schemas on which we will check
    #TODO make not factorial algorith proverki
    cursor.execute("SELECT schema_name FROM information_schema.schemata;")
    schemas = cursor.fetchall()
    schemas = [x[0] for x in schemas if x[0] not in ('pg_catalog', 'information_schema')]
    db_connection.rollback()
    cursor.close()
    for schema in schemas:
        compare_answers_except(student_query, query_template_start, i_q, query_template_end, db_connection, schema)
        db_connection.rollback()
        if (len(i_q) < 1):
            raise Exception('Not correct')

    
    
    
    
    
def compare_answers_except(
    student_query: str,
    query_template_start: str,
    i_q: List[tuple], 
    query_template_end: str,
    db_connection,
    schema: List[tuple]):

    cursor = db_connection.cursor()
    try:
        cursor.execute(f'SET search_path TO {schema};')
    except Exception as err:
        print(err)
        raise err
    
    for combination in list(i_q):
        cor_answ = query_template_start + ' ' + ','.join(combination) + ' ' + query_template_end
        try:
            q_1 = f"""({cor_answ})
            EXCEPT
            ({student_query})"""
            cursor.execute(q_1)
            answer_1 = cursor.fetchall()
            cursor.execute(f"""({student_query}) 
            EXCEPT
            ({cor_answ})""")
            answer_2 = cursor.fetchall()
            if len(answer_1) != 0 or len(answer_2) != 0:
                i_q.remove(combination)        
        except bd_errors.DatatypeMismatch as err:
            print(type(err), err)
            i_q.remove(combination)
            db_connection.rollback()
            #cursor.execute(f'SET search_path TO {schema};')
        except (Exception, psycopg2.DatabaseError) as err:
            print(type(err),err)
            cursor.close()
            raise (err)

    cursor.close()


def main():
    query = '''select    p.town   townp,  j.town  townj, spj.kol,  p.ves
    from  spj
    join  p  on  p.n_det = spj.n_det
    join  j  on  j.n_izd = spj.n_izd
    order  by 1,3      ;'''
    # statetments = sqlparse.split(query)
    # print([x for x in statetments if x!= ';'])
    # print(query[:-1])
    check_query(query, query)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)